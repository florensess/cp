module com.example.computer {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires java.desktop;
    requires javafx.swing;
    requires geocoder.java;
    requires google.maps.services;
    requires de.jensd.fx.glyphs.fontawesome;
    requires de.jensd.fx.glyphs.commons;

    exports controllers;
    opens controllers to javafx.fxml;
    exports models;
    opens models to javafx.base;
    exports table;
    opens table to javafx.base;
}