package controllers;

import client.Client;
import config.DateConfig;
import config.FormValidator;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Account;
import models.Person;
import mvc.AccountStatus;
import mvc.Request;
import org.kordamp.bootstrapfx.BootstrapFX;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AutorizationMenuController {

    private Stage stage;

    public void Show(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/templates/AutorizationMenu.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent parent = loader.getRoot();
        Scene scene = new Scene(parent);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField Au_login;

    @FXML
    private PasswordField Au_password;

    @FXML
    private Label arlet_message;

    @FXML
    private AnchorPane block_autorization;

    @FXML
    private AnchorPane block_registration;

    @FXML
    private Button but_downloadPhoto;

    @FXML
    private Button but_enter;

    @FXML
    private Button but_registration;

    @FXML
    private TextField country__account;

    @FXML
    private TextField login_account;

    @FXML
    private FontAwesomeIconView menu_bar;

    @FXML
    private TextField name_account;

    @FXML
    private TextField password_account;

    @FXML
    private TextField patronymic__account;

    @FXML
    private TextField phone_account;

    @FXML
    private ImageView photo_account;

    @FXML
    private FontAwesomeIconView secCheck;

    @FXML
    private TextField surNamel_account;

    @FXML
    private HBox tool_autorization;

    @FXML
    private VBox tool_bar;

    @FXML
    private TextField date_account;

    @FXML
    private Label tool_exit;

    @FXML
    private HBox tool_registration;

    private File FilePersonalPhoto=new File(System.getProperty("user.dir")+"/src/main/resources/static/images/Noname.png");


    @FXML
    void initialize() {
        but_enter.setOnAction(actionEvent -> {
            FormValidator formValidator=new FormValidator();
            boolean trigger=false;
            if(Au_login.getText().equals("")||Au_password.getText().length()<2){formValidator.validationInput(Au_password,"");trigger=true;}
            if(Au_password.getText().equals("")||Au_login.getText().length()<2){formValidator.validationInput(Au_login,"");trigger=true;}
            if(trigger==false){
                try {
                    Client.getInstance().getOut().writeObject(Request.AUTORIZATION);
                    Client.getInstance().getOut().writeObject(Au_login.getText());
                    Client.getInstance().getOut().writeObject(Au_password.getText());
                    Request messages=(Request) Client.getInstance().getIn().readObject();
                    if(messages==Request.AUTORIZATION_NOT_FOUND_ACCOUNT){
                        arlet_message.setText("Аккаунт не найден");
                        formValidator.viseableNode(arlet_message);
                    }
                    else {
                        messages=(Request) Client.getInstance().getIn().readObject();
                        if(messages!=Request.AUTORIZATION_BLOCK_ACCOUNT){
                            if(messages==Request.AUTORIZATION_ADMIN){
                                but_enter.getScene().getWindow().hide();
                                AdminMenuController adminMenuController=new AdminMenuController();
                                adminMenuController.Show();
                            }
                            if(messages==Request.AUTORIZATION_USER){
                                but_enter.getScene().getWindow().hide();
                                UserMenuController userMenuController=new UserMenuController();
                                userMenuController.Show();
                            }
                        }
                        else {
                            arlet_message.setText("Ваш аккаунт был заблокирован");
                            formValidator.viseableNode(arlet_message);
                        }
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            else {
                formValidator.disabledComponent(but_enter);
            }
        });
        but_registration.setOnAction(actionEvent -> {
            boolean trigger=false;
            FormValidator formValidator=new FormValidator();
            if(!name_account.getText().matches("[ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэюбьтимсчяёЁ]{2,40}")){formValidator.validationInput(name_account,"Имя не может сожержать цифры");trigger=true;}
            if(!surNamel_account.getText().matches("[ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэюбьтимсчяёЁ]{2,40}")){formValidator.validationInput(surNamel_account,"Фамилия не может сожержать цифры");trigger=true;}
            if(!patronymic__account.getText().matches("[ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэюбьтимсчяёЁ]{2,40}")){formValidator.validationInput(patronymic__account,"Отчество не может сожержать цифры");trigger=true;}
            if(password_account.getText().equals("")||password_account.getText().length()<2){formValidator.validationInput(password_account,"Пароль должен быть более 2 символов");trigger=true;}
            if(login_account.getText().equals("")||login_account.getText().length()<2){formValidator.validationInput(login_account,"Логин должен быть более 2 символов");trigger=true;}
            if(!phone_account.getText().matches("(\\+375|80)\\d{9}")){
                formValidator.validationInput(phone_account,"+375 или 80 29,44,25.. xxxxxxx");trigger=true;}
            if(trigger){
                formValidator.disabledComponent(but_registration);
            }
            else {
                try {
                    ImageConfig imageConfig=new ImageConfig();
                    Account account=new Account();
                    account.setPassword(password_account.getText());
                    account.setLogin(login_account.getText());
                    account.setPhoto(imageConfig.SerializableImage(FilePersonalPhoto));
                    Person person = new Person();
                    person.setCountry("Минск");
                    person.setName(name_account.getText());
                    person.setSur_name(surNamel_account.getText());
                    person.setPatronymic(patronymic__account.getText());
                    person.setPhone(phone_account.getText());
                    Client.getInstance().getOut().writeObject(Request.REGISTRATION_ACCOUNT);
                    Client.getInstance().getOut().writeObject(account);
                    Client.getInstance().getOut().writeObject(person);
                    formValidator.visableIcon(secCheck);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        menu_bar.setOnMouseClicked(mouseEvent -> {
            if(menu_bar.getGlyphName().equals("ALIGN_JUSTIFY")){
                menu_bar.setIcon(FontAwesomeIcon.ALIGN_LEFT);
                tool_bar.getStyleClass().remove("v-box");
                tool_bar.getStyleClass().add("v-box-open");

            }
            else {
                menu_bar.setIcon(FontAwesomeIcon.ALIGN_JUSTIFY);
                tool_bar.getStyleClass().remove("v-box-open");
                tool_bar.getStyleClass().add("v-box");
            }
        });
        tool_autorization.setOnMouseClicked(mouseEvent -> {
            if(tool_registration.getStyleClass().contains("h-box_open"))tool_registration.getStyleClass().remove("h-box_open");
            if(tool_autorization.getStyleClass().contains("h-box_open"))tool_autorization.getStyleClass().remove("h-box_open");
            tool_autorization.getStyleClass().add("h-box_open");
            block_autorization.setVisible(true);
            block_registration.setVisible(false);
        });
        tool_registration.setOnMouseClicked(mouseEvent -> {
            date_account.setText(DateConfig.getDateConfigure().nowDate());
            if(tool_autorization.getStyleClass().contains("h-box_open"))tool_autorization.getStyleClass().remove("h-box_open");
            if(tool_registration.getStyleClass().contains("h-box_open"))tool_registration.getStyleClass().remove("h-box_open");
            tool_registration.getStyleClass().add("h-box_open");
            block_autorization.setVisible(false);
            block_registration.setVisible(true);
        });
        but_downloadPhoto.setOnAction(actionEvent -> {
            FileChooser fileChooser=new FileChooser();
            fileChooser.setTitle("Выберите фото");
            FileChooser.ExtensionFilter filter=new FileChooser.ExtensionFilter("Select ...",".png",".jpg");
            fileChooser.getExtensionFilters()
                    .addAll(new FileChooser.ExtensionFilter("Select ...","*.png","*.jpg"));
            FilePersonalPhoto=fileChooser.showOpenDialog(stage);
            if(FilePersonalPhoto!=null){
                Image image=new Image("file:///"+FilePersonalPhoto.getAbsolutePath());
                photo_account.setImage(image);
            }
        });

    }
}
