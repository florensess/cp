package controllers;

import client.Client;
import config.DateConfig;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Account;
import models.Person;
import mvc.AccountStatus;
import mvc.Request;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PersonalAccountController {

    private Stage stage;

    public  void Show(){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/templates/AdminMenu/PersonalAccount.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent parent = loader.getRoot();
        Scene scene=new Scene(parent);
        stage=new Stage();
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button TakeAdmin;

    @FXML
    private AnchorPane block_acc;

    @FXML
    private AnchorPane block_ban;

    @FXML
    private Button but_blockOrUnblock;

    @FXML
    private Button but_cancel;

    @FXML
    private FontAwesomeIconView but_close;

    @FXML
    private Button but_send;

    @FXML
    private TextField country;

    @FXML
    private TextField dateRegistration;

    @FXML
    private TextField date_ban;

    @FXML
    private TextField login;

    @FXML
    private TextField name;

    @FXML
    private TextField patronymic;

    @FXML
    private TextField phone;

    @FXML
    private ImageView photo;

    @FXML
    private Button removeAcc;

    @FXML
    private TextField role;

    @FXML
    private TextField status;

    @FXML
    private TextField surname;

    @FXML
    private TextArea text_rep;

    private int id_account;

    private void initialized() throws IOException, ClassNotFoundException {
        ImageConfig imageConfig=new ImageConfig();
        Account account=(Account) Client.getInstance().getIn().readObject();
        Person person=(Person) Client.getInstance().getIn().readObject();
        Request request=(Request)Client.getInstance().getIn().readObject();
        name.setText(person.getName());
        surname.setText(person.getSur_name());
        patronymic.setText(person.getPatronymic());
        country.setText(person.getCountry());
        phone.setText(person.getPhone());
        dateRegistration.setText(account.getDate_registration());
        status.setText(account.getStatus());
        role.setText(account.getRole());
        photo.setImage(imageConfig.DeserializableImage(account.getPhoto()));
        id_account=account.getId();
        if(request==Request.YOUR_ACCOUNT){
            but_blockOrUnblock.setDisable(true);
            removeAcc.setDisable(true);
            TakeAdmin.setDisable(true);
        }
        else {
            if(account.getStatus().equals(AccountStatus.blockAccount)){
                but_blockOrUnblock.setText("Разблокировать");
                but_blockOrUnblock.getStyleClass().add("but_sec");
                status.getStyleClass().remove("text-field_histroy");
                status.getStyleClass().add("text-field_dange");
            }
            else {
                but_blockOrUnblock.setText("Заблокировать");
                but_blockOrUnblock.getStyleClass().add("but_dange");
            }
            if(role.getText().equals(AccountStatus.admin)){
                TakeAdmin.setText("Забарть права администратора");
            }
            else {
                TakeAdmin.setText("Сделать администратором");
            }
        }
    }

    @FXML
    void initialize() throws IOException, ClassNotFoundException {
       initialized();
       TakeAdmin.setOnAction(actionEvent -> {
           if(TakeAdmin.getText().equals("Сделать администратором")){
               TakeAdmin.setText("Забрать права администратора");
               role.setText(AccountStatus.admin);
               TakeAdmin.getStyleClass().remove("but_sec");
               TakeAdmin.getStyleClass().add("but_dange");
               try {
                   Client.getInstance().getOut().writeObject(Request.TAKE_ADMIN);
                   Client.getInstance().getOut().writeObject(id_account);
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
           else {
               TakeAdmin.setText("ЗСделать администратором");
               role.setText(AccountStatus.user);
               TakeAdmin.getStyleClass().remove("but_dange");
               TakeAdmin.getStyleClass().add("but_sec");
               try {
                   Client.getInstance().getOut().writeObject(Request.REMOVE_ROOL_ADMIN);
                   Client.getInstance().getOut().writeObject(id_account);
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       });
       but_cancel.setOnAction(actionEvent -> {
           block_acc.setVisible(true);
           block_ban.setVisible(false);
           text_rep.setText("");
       });
       but_send.setOnAction(actionEvent -> {
           try {
               Client.getInstance().getOut().writeObject(Request.BLOCK_ACCOUNT);
               Client.getInstance().getOut().writeObject(id_account);
               Client.getInstance().getOut().writeObject(text_rep.getText());
               status.setText(AccountStatus.blockAccount);
               status.getStyleClass().remove("text-field_histroy");
               status.getStyleClass().add("text-field_dange");
               but_blockOrUnblock.setText("Разблокировать");
               but_blockOrUnblock.getStyleClass().remove("but_dange");
               but_blockOrUnblock.getStyleClass().add("but_sec");
               block_ban.setVisible(false);
               block_acc.setVisible(true);
           } catch (IOException e) {
               e.printStackTrace();
           }
       });
       but_blockOrUnblock.setOnAction(actionEvent -> {
           if(but_blockOrUnblock.getText().equals("Заблокировать")){
               block_acc.setVisible(false);
               block_ban.setVisible(true);
               date_ban.setText(DateConfig.getDateConfigure().nowDate());
           }
           else {
               try {
                   Client.getInstance().getOut().writeObject(Request.UNBLOCK_ACCOUNT);
                   Client.getInstance().getOut().writeObject(id_account);
                   status.setText(AccountStatus.activeAccount);
                   status.getStyleClass().remove("text-field_dange");
                   status.getStyleClass().add("text-field_histroy");
                   but_blockOrUnblock.setText("Заблокировать");
                   but_blockOrUnblock.getStyleClass().remove("but_sec");
                   but_blockOrUnblock.getStyleClass().add("but_dange");
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       });
       removeAcc.setOnAction(actionEvent -> {
           try {
               Client.getInstance().getOut().writeObject(Request.REMOVE_ACCOUNT);
               Client.getInstance().getOut().writeObject(id_account);
               status.setText("Аккаунт удален !");
               status.getStyleClass().remove(0);
               status.getStyleClass().add("text-field_dange");
           } catch (IOException e) {
               e.printStackTrace();
           }
       });
       but_close.setOnMouseClicked(mouseEvent -> {
            but_close.getScene().getWindow().hide();
        });
    }

}
