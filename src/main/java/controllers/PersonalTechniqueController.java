package controllers;

import client.Client;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.*;
import mvc.ProductStatus;
import mvc.Request;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PersonalTechniqueController {

    private Stage stage;

    public  void Show(){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/templates/AdminMenu/PersonalTechnique.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent parent = loader.getRoot();
        Scene scene=new Scene(parent);
        stage=new Stage();
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField CPU;

    @FXML
    private TextField Color;

    @FXML
    private TextField Memory;

    @FXML
    private TextField Power;

    @FXML
    private TextField PowerMark;

    @FXML
    private TextField TypeProd;

    @FXML
    private TextField Video_frequencyCPU;

    @FXML
    private TextField Video_memory_frequency;

    @FXML
    private TextField Video_memory_size;

    @FXML
    private TextField Video_type;

    @FXML
    private AnchorPane block_computer;

    @FXML
    private AnchorPane block_power;

    @FXML
    private AnchorPane block_processor;

    @FXML
    private AnchorPane block_videoCard;

    @FXML
    private FontAwesomeIconView but_close;

    @FXML
    private TextField clock_frequencyCPU;

    @FXML
    private TextField dateProd;

    @FXML
    private TextField fullpriceProd;

    @FXML
    private TextField nameProd;

    @FXML
    private TextField numberCoreCPU;

    @FXML
    private TextField numberProd;

    @FXML
    private TextField powerCPU;

    @FXML
    private TextField powerV;

    @FXML
    private TextField priceProd;

    @FXML
    private TextField statusProd;

    @FXML
    private TextField videoCard;

    @FXML
    private ImageView photo;

    @FXML
    private TextField workCPU;

    @FXML
    private Button but_del;

    @FXML
    private Button but_stop;

    @FXML
    private Button but_change;

    private int idTech;

    private void initialized() throws IOException, ClassNotFoundException {
        ImageConfig imageConfig=new ImageConfig();
        Technique technique=(Technique) Client.getInstance().getIn().readObject();
        idTech=technique.getId();
        nameProd.setText(technique.getName());
        statusProd.setText(technique.getStatus());
        photo.setImage(imageConfig.DeserializableImage(technique.getPhoto()));
        numberProd.setText(Double.toString(technique.getNumber()));
        priceProd.setText(Double.toString(technique.getPrice()));
        fullpriceProd.setText(Double.toString(technique.getPrice()*technique.getNumber()));
        dateProd.setText(technique.getDate_reg());
        TypeProd.setText(technique.getType());
        if(technique.getType().equals(ProductStatus.computerProduct)){
            block_computer.setVisible(true);
            block_power.setVisible(false);
            block_processor.setVisible(false);
            block_videoCard.setVisible(false);
            Computer computer=(Computer) Client.getInstance().getIn().readObject();
            videoCard.setText(computer.getVideo_card());
            CPU.setText(computer.getCpu());
            Power.setText(computer.getPower_unit());
            Color.setText(computer.getColor());
            Memory.setText(computer.getMemory());
        }
        if(technique.getType().equals(ProductStatus.computerVideo)){
            VideoCard videoCard=(VideoCard)Client.getInstance().getIn().readObject();
            Video_memory_size.setText(videoCard.getMemory_size());
            Video_frequencyCPU.setText(Double.toString(videoCard.getFrequency_GPU()));
            Video_memory_frequency.setText(Double.toString(videoCard.getFrequency_memory()));
            Video_type.setText(videoCard.getType_memory());
            block_computer.setVisible(false);
            block_power.setVisible(false);
            block_processor.setVisible(false);
            block_videoCard.setVisible(true);
        }
        if(technique.getType().equals(ProductStatus.computerCPU)){
            Cpu cpu=(Cpu)Client.getInstance().getIn().readObject();
            clock_frequencyCPU.setText(Double.toString(cpu.getClock_frequency()));
            workCPU.setText(Double.toString(cpu.getPerformance()));
            powerCPU.setText(Double.toString(cpu.getEnergy_consumption()));
            numberCoreCPU.setText(Integer.toString(cpu.getNumber_cores()));
            block_computer.setVisible(false);
            block_power.setVisible(false);
            block_processor.setVisible(true);
            block_videoCard.setVisible(false);
        }
        if(technique.getType().equals(ProductStatus.computerPower)){
            PowerUnit powerUnit=(PowerUnit) Client.getInstance().getIn().readObject();
            PowerMark.setText(powerUnit.getMark());
            powerV.setText(Integer.toString(powerUnit.getNumber_volts()));
            block_computer.setVisible(false);
            block_power.setVisible(true);
            block_processor.setVisible(false);
            block_videoCard.setVisible(false);
        }
    }

    @FXML
    void initialize() throws IOException, ClassNotFoundException {
        initialized();
        but_del.setOnAction(actionEvent -> {
            try {
                Client.getInstance().getOut().writeObject(Request.REMOVE_PRODUCT);
                Client.getInstance().getOut().writeObject(idTech);
                statusProd.getStyleClass().remove("text-field_histroy");
                statusProd.getStyleClass().add("text-field_dange");
                but_change.setDisable(true);
                but_stop.setDisable(true);
                statusProd.setText("Аккаунт удален !");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        but_close.setOnMouseClicked(mouseEvent -> {
            but_close.getScene().getWindow().hide();
        });
    }
}