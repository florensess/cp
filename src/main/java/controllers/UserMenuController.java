package controllers;

import client.Client;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.Account;
import models.Technique;
import mvc.ProductStatus;
import mvc.Request;
import org.kordamp.bootstrapfx.BootstrapFX;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class UserMenuController {
    private Stage stage;

    public void Show(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/templates/UserMenu/UserMenu.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent parent = loader.getRoot();
        Scene scene = new Scene(parent);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField PrCPUReg;

    @FXML
    private TextField PrColotReg;

    @FXML
    private TextField PrPowerReg;

    @FXML
    private TextField PrPowerReg1;

    @FXML
    private TextField PrVideoCardReg;

    @FXML
    private AnchorPane anchListTech;

    @FXML
    private AnchorPane block_computer;

    @FXML
    private AnchorPane block_techcnica;

    @FXML
    private FontAwesomeIconView menu_bar;

    @FXML
    private HBox tool_basket;

    @FXML
    private HBox tool_computer;

    @FXML
    private HBox tool_exit;

    @FXML
    private HBox tool_history;

    @FXML
    private HBox tool_settins;

    @FXML
    private VBox tools_bar;

    private VBox generalChar(Technique technique){

        VBox vBox=new VBox();
        vBox.setLayoutY(14);
        vBox.setLayoutX(259);
        vBox.setPadding(new Insets(-2,-2,-2,-2));
        vBox.setPrefWidth(330);
        vBox.setPrefHeight(210);
        vBox.getStyleClass().add("box_group");
        Label labelHeader=new Label("Сведенья о товаре");
        labelHeader.setAlignment(Pos.CENTER);
        labelHeader.setPrefWidth(333);
        labelHeader.setPrefHeight(27);
        labelHeader.getStyleClass().add("label_header_background");

        HBox hBoxName=new HBox();
        hBoxName.setAlignment(Pos.CENTER_LEFT);
        hBoxName.setPadding(new Insets(0,0,0,20));
        hBoxName.setStyle("-fx-background-color:  #1f2c36");
        hBoxName.setPrefHeight(34);
        TextField textFieldName=new TextField(technique.getName());
        textFieldName.setCursor(Cursor.DEFAULT);
        textFieldName.setEditable(false);
        textFieldName.setPrefHeight(240);
        textFieldName.setPrefHeight(19);
        textFieldName.setAlignment(Pos.CENTER);
        textFieldName.getStyleClass().add("text-field_histroy");
        Label label1=new Label("Наименование");
        label1.getStyleClass().add("label_signature");
        HBox.setMargin(textFieldName,new Insets(0,0,0,20));
        hBoxName.getChildren().add(label1);
        hBoxName.getChildren().add(textFieldName);

        HBox hBoxNumber=new HBox();
        hBoxNumber.setAlignment(Pos.CENTER_LEFT);
        hBoxNumber.setPadding(new Insets(0,0,0,20));
        hBoxNumber.setStyle("-fx-background-color:  #1f2c36");
        hBoxNumber.setPrefHeight(34);
        TextField textFieldNumber=new TextField(Integer.toString(technique.getNumber()));
        textFieldNumber.setEditable(false);
        textFieldNumber.setCursor(Cursor.DEFAULT);
        textFieldNumber.setPrefWidth(150);
        textFieldNumber.setPrefHeight(19);
        textFieldNumber.setAlignment(Pos.CENTER_RIGHT);
        textFieldNumber.getStyleClass().add("text-field_histroy");
        Label label3=new Label("В наличии");
        label3.getStyleClass().add("label_signature");
        Label label4=new Label("Штук");
        label4.getStyleClass().add("label_signature");
        HBox.setMargin(textFieldNumber,new Insets(0,5,0,20));
        hBoxNumber.getChildren().add(label3);
        hBoxNumber.getChildren().add(textFieldNumber);
        hBoxNumber.getChildren().add(label4);

        HBox hBoxDate=new HBox();
        hBoxDate.setAlignment(Pos.CENTER_LEFT);
        hBoxDate.setPadding(new Insets(0,0,0,20));
        hBoxDate.setStyle("-fx-background-color:  #1f2c36");
        hBoxDate.setPrefHeight(34);
        TextField textFieldDate=new TextField(technique.getDate_reg());
        textFieldDate.setCursor(Cursor.DEFAULT);
        textFieldDate.setEditable(false);
        textFieldDate.setPrefHeight(190);
        textFieldDate.setPrefHeight(19);
        textFieldDate.setAlignment(Pos.CENTER);
        textFieldDate.getStyleClass().add("text-field_histroy");
        Label label5=new Label("Дата поступления");
        label5.getStyleClass().add("label_signature");
        HBox.setMargin(textFieldDate,new Insets(0,0,0,20));
        hBoxDate.getChildren().add(label5);
        hBoxDate.getChildren().add(textFieldDate);

        HBox hBoxStatus=new HBox();
        hBoxStatus.setAlignment(Pos.CENTER_LEFT);
        hBoxStatus.setPadding(new Insets(0,0,0,20));
        hBoxStatus.setStyle("-fx-background-color:  #1f2c36");
        hBoxStatus.setPrefHeight(34);
        TextField textFieldStatus=new TextField(technique.getStatus());
        textFieldStatus.setCursor(Cursor.DEFAULT);
        textFieldStatus.setEditable(false);
        textFieldStatus.setPrefHeight(190);
        textFieldStatus.setPrefHeight(19);
        textFieldStatus.setAlignment(Pos.CENTER);
        if(technique.getStatus().equals(ProductStatus.noProduct)) textFieldStatus.getStyleClass().add("text-field_dange");
        else textFieldStatus.getStyleClass().add("text-field_histroy");
        Label label6=new Label("Статус");
        label6.getStyleClass().add("label_signature");
        HBox.setMargin(textFieldStatus,new Insets(0,0,0,20));
        hBoxStatus.getChildren().add(label6);
        hBoxStatus.getChildren().add(textFieldStatus);

        HBox hBoxPrice=new HBox();
        hBoxPrice.setAlignment(Pos.CENTER_LEFT);
        hBoxPrice.setPadding(new Insets(0,0,0,20));
        hBoxPrice.setStyle("-fx-background-color:  #1f2c36");
        hBoxPrice.setPrefHeight(34);
        TextField textFieldPrice=new TextField(Double.toString(technique.getPrice()));
        textFieldPrice.setCursor(Cursor.DEFAULT);
        textFieldPrice.setEditable(false);
        textFieldPrice.setPrefHeight(155);
        textFieldPrice.setPrefHeight(19);
        textFieldPrice.setAlignment(Pos.CENTER);
        textFieldPrice.getStyleClass().add("text-field_histroy");
        Label label7=new Label("Цена за штуку");
        label7.getStyleClass().add("label_signature");
        Label label8=new Label("BY");
        label8.getStyleClass().add("label_signature");
        HBox.setMargin(textFieldPrice,new Insets(0,5,0,20));
        hBoxPrice.getChildren().add(label7);
        hBoxPrice.getChildren().add(textFieldPrice);
        hBoxPrice.getChildren().add(label8);

        HBox hBoxType=new HBox();
        hBoxType.setAlignment(Pos.CENTER_LEFT);
        hBoxType.setPadding(new Insets(0,0,0,20));
        hBoxType.setStyle("-fx-background-color:  #1f2c36");
        hBoxType.setPrefHeight(34);
        TextField textFieldType=new TextField(technique.getType());
        textFieldType.setCursor(Cursor.DEFAULT);
        textFieldType.setEditable(false);
        textFieldType.setPrefWidth(233);
        textFieldType.setPrefHeight(19);
        textFieldType.getStyleClass().add("text-field_histroy");
        textFieldType.setAlignment(Pos.CENTER);
        Label label9=new Label("Раздел");
        label9.getStyleClass().add("label_signature");
        HBox.setMargin(textFieldType,new Insets(0,5,0,20));
        hBoxType.getChildren().add(label9);
        hBoxType.getChildren().add(textFieldType);

        vBox.getChildren().add(labelHeader);
        vBox.getChildren().add(hBoxName);
        vBox.getChildren().add(hBoxNumber);
        vBox.getChildren().add(hBoxDate);
        vBox.getChildren().add(hBoxStatus);
        vBox.getChildren().add(hBoxPrice);
        vBox.getChildren().add(hBoxType);
        return vBox;
    }
    private VBox techBuy(Technique technique){
        VBox vBoxBuy=new VBox();
        vBoxBuy.setPadding(new Insets(-2,-2,0,-2));
        vBoxBuy.setLayoutX(610);
        vBoxBuy.setLayoutY(14);
        vBoxBuy.setPrefWidth(180);
        vBoxBuy.setPrefHeight(210);
        vBoxBuy.getStyleClass().add("box_group");
        Label labelHeader1=new Label("Покупка");
        labelHeader1.setAlignment(Pos.CENTER);
        labelHeader1.setPrefWidth(333);
        labelHeader1.setPrefHeight(27);
        labelHeader1.getStyleClass().add("label_header_background");

        HBox hBoxLabelBuy=new HBox();
        hBoxLabelBuy.setAlignment(Pos.CENTER);
        hBoxLabelBuy.setPrefHeight(23);
        Label label2=new Label("Количество");
        label2.getStyleClass().add("label_signature");
        hBoxLabelBuy.getChildren().add(label2);

        HBox hBoxNumber=new HBox();
        hBoxNumber.setAlignment(Pos.CENTER);
        hBoxNumber.setPrefHeight(38);
        SpinnerValueFactory<Integer> number =new SpinnerValueFactory.IntegerSpinnerValueFactory(1, technique.getNumber(), 1);
        Spinner<Integer>spinnerNumber=new Spinner<>();
        spinnerNumber.setValueFactory(number);
        hBoxNumber.getChildren().add(spinnerNumber);

        HBox hboxPrice=new HBox();
        hboxPrice.setPrefHeight(38);
        hboxPrice.setAlignment(Pos.CENTER);
        Label label3=new Label("Итог");
        label3.getStyleClass().add("label_signature");
        label3.setStyle("-fx-end-margin: 20px");
        Label label4=new Label(Double.toString(technique.getPrice()));
        label4.getStyleClass().add("label_price");
        label4.setPrefWidth(108);
        label4.setAlignment(Pos.CENTER_RIGHT);
        label4.setPadding(new Insets(5,5,5,5));
        Label label5=new Label("BY");
        label5.getStyleClass().add("label_signature");
        HBox.setMargin(label5,new Insets(0,0,0,5));
        HBox.setMargin(label3,new Insets(0,10,0,0));
        hboxPrice.getChildren().add(label3);
        hboxPrice.getChildren().add(label4);
        hboxPrice.getChildren().add(label5);

        HBox hBoxButton=new HBox();
        hBoxButton.setPrefHeight(80);
        hBoxButton.setAlignment(Pos.CENTER);
        Button button=new Button("Добавить в корзину");
        hBoxButton.getChildren().add(button);

        if(ProductStatus.noProduct.equals(technique.getStatus())){
            spinnerNumber.setDisable(true);
            button.setDisable(true);
        }



        spinnerNumber.valueProperty().addListener(new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observableValue, Integer integer, Integer t1) {
                String val=Double.toString(technique.getPrice()*spinnerNumber.getValue());
               label4.setText(val.substring(0,val.indexOf(".")+3));
            }
        });

        vBoxBuy.getChildren().add(labelHeader1);
        vBoxBuy.getChildren().add(hBoxLabelBuy);
        vBoxBuy.getChildren().add(hBoxNumber);
        vBoxBuy.getChildren().add(hboxPrice);
        vBoxBuy.getChildren().add(hBoxButton);

        return vBoxBuy;
    }

    private void initializdTech() throws IOException, ClassNotFoundException {
        List<Technique> list=(List<Technique>) Client.getInstance().getIn().readObject();
        double x=9;
        double y=14;
        for(int i=0;i<list.size();i++){
            ImageConfig imageConfig=new ImageConfig();

            if (i!=0){
                y=y+376;
            }
            AnchorPane achorTech=new AnchorPane();
            achorTech.setPrefWidth(806);
            achorTech.setPrefHeight(366);
            achorTech.setLayoutX(x);
            achorTech.setLayoutY(y);
            achorTech.getStyleClass().add("box_group");

            VBox vBoxImage=new VBox();
            vBoxImage.setAlignment(Pos.CENTER);
            vBoxImage.setPrefHeight(210);
            vBoxImage.setPrefWidth(210);
            vBoxImage.setLayoutY(14);
            vBoxImage.setLayoutX(24);
            vBoxImage.getStyleClass().add("box_group");

            ImageView imageView=new ImageView(imageConfig.DeserializableImage(list.get(i).getPhoto()));
            imageView.setFitWidth(185);
            imageView.setFitHeight(185);
            vBoxImage.getChildren().add(imageView);





            achorTech.getChildren().add(vBoxImage);
            achorTech.getChildren().add(techBuy(list.get(i)));
            achorTech.getChildren().add(generalChar(list.get(i)));
            anchListTech.getChildren().add(achorTech);
        }
    }

    @FXML
    void initialize() throws IOException, ClassNotFoundException {
        initializdTech();
        menu_bar.setOnMouseClicked(mouseEvent -> {
            if(menu_bar.getGlyphName().equals("ALIGN_JUSTIFY")){
                menu_bar.setIcon(FontAwesomeIcon.ALIGN_LEFT);
                tools_bar.getStyleClass().remove("v-box");
                tools_bar.getStyleClass().add("v-box-open");

            }
            else {
                menu_bar.setIcon(FontAwesomeIcon.ALIGN_JUSTIFY);
                tools_bar.getStyleClass().remove("v-box-open");
                tools_bar.getStyleClass().add("v-box");
            }
        });
        tool_exit.setOnMouseClicked(mouseEvent -> {
            tool_exit.getScene().getWindow().hide();
            try {
                Client.getInstance().getOut().writeObject(Request.EXIT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            AutorizationMenuController autorizationMenuController=new AutorizationMenuController();
            autorizationMenuController.Show();
        });

    }

}
