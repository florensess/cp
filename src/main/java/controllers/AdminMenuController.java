package controllers;

import client.Client;
import config.FormValidator;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.*;
import mvc.Request;
import org.kordamp.bootstrapfx.BootstrapFX;
import table.TableAcPe;
import table.TableBan;
import table.TableTeq;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdminMenuController {

    private Stage stage;

    public void Show(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/templates/AdminMenu/AdminMenu.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent parent = loader.getRoot();
        Scene scene = new Scene(parent);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
        stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private AnchorPane block_account;

    @FXML
    private AnchorPane block_blackList;

    @FXML
    private AnchorPane block_computer;

    @FXML
    private AnchorPane block_history;

    @FXML
    private AnchorPane block_report;

    @FXML
    private AnchorPane block_settins;

    @FXML
    private AnchorPane block_statistic;

    @FXML
    private FontAwesomeIconView labelToolAccount;

    @FXML
    private FontAwesomeIconView menu_bar;

    @FXML
    private AnchorPane secBlock_list_technica;

    @FXML
    private AnchorPane secBlock_reg_technica;

    @FXML
    private Button setB_save;

    @FXML
    private FontAwesomeIconView setI_sec;

    @FXML
    private TextField set_country;

    @FXML
    private TextField set_dateRegistrated;

    @FXML
    private TextField set_name;

    @FXML
    private TextField set_patronymic;

    @FXML
    private TextField set_phone;

    @FXML
    private TextField set_status;

    @FXML
    private TextField set_surName;

    @FXML
    private HBox tool_account;

    @FXML
    private HBox tool_blackLIst;

    @FXML
    private HBox tool_computer;

    @FXML
    private HBox tool_exit;

    @FXML
    private HBox tool_history;

    @FXML
    private HBox tool_report;

    @FXML
    private TextField set_login;

    @FXML
    private TextField set_password;

    @FXML
    private HBox tool_settins;

    @FXML
    private HBox tool_statistic;

    @FXML
    private TableColumn<TableAcPe, Integer> colAc_Id;

    @FXML
    private TableColumn<TableAcPe, String> colAc_photo;

    @FXML
    private TableColumn<TableAcPe, String> colAc_login;

    @FXML
    private TableColumn<TableAcPe,String>colAc_phone;

    @FXML
    private TableColumn<TableAcPe, String> colAc_status;

    @FXML
    private TableColumn<TableAcPe, String> colAc_role;

    @FXML
    private TableColumn<TableAcPe, String> colAc_edition;

    @FXML
    private TableColumn<TableAcPe, String> colAc_dateReg;

    @FXML
    private TableView<TableAcPe>table_account;

    @FXML
    private ImageView setPhoto;

    @FXML
    private VBox tools_bar;

    @FXML
    private ToggleButton tougleListComp;

    @FXML
    private Button setB_downloadPhoto;

    @FXML
    private AnchorPane box_computer;

    @FXML
    private AnchorPane box_video;

    @FXML
    private ComboBox<String>PrTypeReg;

    @FXML
    private ImageView photoComp;

    @FXML
    private ToggleButton tougleRegComp;

    @FXML
    private FontAwesomeIconView sec_addTech;

    @FXML
    private Button but_addTech;

    @FXML
    private AnchorPane box_processor;

    @FXML
    private Button but_downloadComp;

    @FXML
    private Spinner<Integer>numberTech;

    @FXML
    private AnchorPane box_blockUp;

    @FXML
    private TextField PrNameReg;

    @FXML
    private TextField PrPriceReg;

    @FXML
    private TextField PrVideoCardReg;

    @FXML
    private TextField PrCPUReg;

    @FXML
    private ComboBox<String> PrMNuberReg;

    @FXML
    private TextField PrPowerReg;

    @FXML
    private ComboBox<String>PrMS_Video;

    @FXML
    private TextField PrTMemory_Video;

    @FXML
    private Spinner<Integer>RegCPUCore;

    @FXML
    private TextField RegCPUEnergy;

    @FXML
    private TextField RegCPUCF;

    @FXML
    private TextField RegCPUWork;

    @FXML
    private TextField PrGPUF_Video;

    @FXML
    private TableColumn<TableTeq, String> colTechID;

    @FXML
    private TableColumn<TableTeq, String> colTechPhoto;

    @FXML
    private TableColumn<TableTeq, String> colTechName;

    @FXML
    private TableColumn<TableTeq, String> colTechStatus;

    @FXML
    private TableColumn<TableTeq, String> colTechPrice;

    @FXML
    private TableColumn<TableTeq, String> colTechNumber;

    @FXML
    private Label  IndexUserAcc;

    @FXML
    private Label IndexTechAcc;

    @FXML
    private Label IndexAdminAcc;

    @FXML
    private Label IndexBlockAcc;

    @FXML
    private TableColumn<TableTeq, String> colTechType;

    @FXML
    private TableView<TableTeq>tableTech;

    @FXML
    private TextField imp_searchTech;

    @FXML
    private TextField PrMarkP;

    @FXML
    private TextField PrNumberVolt;

    @FXML
    private TextField PrGPUM_Video;

    @FXML
    private TableView<TableBan> banTable;

    @FXML
    private TableColumn<TableBan,String>banColId;

    @FXML
    private TableColumn<TableBan,String>banCollog;

    @FXML
    private TableColumn<TableBan,String>banColPhoto;

    @FXML
    private TableColumn<TableBan,String>banColStatus;

    @FXML
    private TableColumn<TableBan,String>banColReason;

    @FXML
    private TableColumn<TableBan,String>banColdate;

    @FXML
    private TableColumn<TableBan,String>banColedit;

    @FXML
    private TextField PrColotReg;

    private FilteredList<TableTeq> filterTech;
    private  File photoComputer=new File(System.getProperty("user.dir")+"/src/main/resources/static/images/Noname.png");
    private  File photo=null;

    private void initializedBanList() throws IOException, ClassNotFoundException {
        List<BanList>banLists=(List<BanList>) Client.getInstance().getIn().readObject();
        ObservableList<TableBan>ban=FXCollections.observableArrayList();
        for(int i=0;i<banLists.size();i++){
            Client.getInstance().getOut().writeObject(Request.FIND_ACC);
            Client.getInstance().getOut().writeObject(banLists.get(i).getId_account());
            Account account=(Account)Client.getInstance().getIn().readObject();
            TableBan tableBan=new TableBan(banLists.get(i),account);
            ban.add(tableBan);
        }
        banColId.setCellValueFactory(new PropertyValueFactory<TableBan,String>("id"));
        banColPhoto.setCellValueFactory(new PropertyValueFactory<TableBan,String>("photo"));
        banCollog.setCellValueFactory(new PropertyValueFactory<TableBan,String>("login"));
        banColStatus.setCellValueFactory(new PropertyValueFactory<TableBan,String>("status"));
        banColReason.setCellValueFactory(new PropertyValueFactory<TableBan,String>("reason"));
        banColdate.setCellValueFactory(new PropertyValueFactory<TableBan,String>("date"));
        banColedit.setCellValueFactory(new PropertyValueFactory<TableBan,String>("edition"));
        banTable.setItems(ban);
    }

    private void initlizedStatistic() throws IOException, ClassNotFoundException {
        IndexAdminAcc.setText((String) Client.getInstance().getIn().readObject());
        IndexUserAcc.setText((String) Client.getInstance().getIn().readObject());
        IndexBlockAcc.setText((String) Client.getInstance().getIn().readObject());
        IndexTechAcc.setText((String) Client.getInstance().getIn().readObject());
    }

    private void initializedTechnica() throws IOException, ClassNotFoundException {
        List<Technique>list=(List<Technique>) Client.getInstance().getIn().readObject();
        List<TableTeq>listTable=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            TableTeq tableTeq=new TableTeq(list.get(i));
            listTable.add(tableTeq);
        }
        ObservableList<TableTeq>listT= FXCollections.observableList(listTable);
        filterTech=new FilteredList<>(listT, p -> true);
        colTechID.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("id"));
        colTechName.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("name"));
        colTechNumber.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("number"));
        colTechPhoto.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("photo"));
        colTechType.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("type"));
        colTechStatus.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("status"));
        colTechPrice.setCellValueFactory(new PropertyValueFactory<TableTeq,String>("price"));
        tableTech.setItems(listT);
        SortedList<TableTeq> sort= new SortedList<>(filterTech);
        sort.comparatorProperty().bind(tableTech.comparatorProperty());
        tableTech.setItems(sort);

    }

    private void initializedComponent(){
        SpinnerValueFactory<Integer> сore =new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 12, 1);
        SpinnerValueFactory<Integer> Shour =new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100, 1);
        numberTech.setValueFactory(Shour);
        RegCPUCore.setValueFactory(сore);
        PrMNuberReg.getItems().add("8 гб");
        PrMNuberReg.getItems().add("16 гб");
        PrMNuberReg.getItems().add("32 гб");
        PrMNuberReg.getItems().add("64 гб");
        PrMNuberReg.getItems().add("128 гб");
        PrTypeReg.getItems().add("Собранный компьютер");
        PrTypeReg.getItems().add("Видео карта");
        PrTypeReg.getItems().add("Процессор");
        PrTypeReg.getItems().add("Блок питания");
        PrTypeReg.getItems().add("Гарнитура");
        PrMS_Video.getItems().add("256 MB");
        PrMS_Video.getItems().add("512 MB");
        PrMS_Video.getItems().add("1  GB");
        PrMS_Video.getItems().add("2 GB");

    }
    private void initializedAccount() throws IOException, ClassNotFoundException {
        ImageConfig imageConfig=new ImageConfig();
        Account account=(Account) Client.getInstance().getIn().readObject();
        Person person=(Person) Client.getInstance().getIn().readObject();
        List<Account>list=(List<Account>) Client.getInstance().getIn().readObject();
        List<Person>listPerson=(List<Person>)Client.getInstance().getIn().readObject();
        set_name.setText(person.getName());
        set_surName.setText(person.getSur_name());
        set_patronymic.setText(person.getPatronymic());
        set_phone.setText(person.getPhone());
        set_country.setText(person.getCountry());
        set_status.setText(account.getStatus());
        set_login.setText(account.getLogin());
        set_password.setText(account.getPassword());
        set_dateRegistrated.setText(account.getDate_registration());
        setPhoto.setImage(imageConfig.DeserializableImage(account.getPhoto()));
        List<TableAcPe>listTable=new ArrayList<>();
        for(int i=0;i<listPerson.size();i++){
            TableAcPe tableAcPe=new TableAcPe(listPerson.get(i),list.get(i));
            listTable.add(tableAcPe);
        }
        ObservableList<TableAcPe>listAccount= FXCollections.observableList(listTable);
        colAc_photo.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("photo"));
        colAc_dateReg.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("date_registaration"));
        colAc_login.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("login"));
        colAc_Id.setCellValueFactory(new PropertyValueFactory<TableAcPe,Integer>("id_account"));
        colAc_role.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("role"));
        colAc_phone.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("phone"));
        colAc_status.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("status"));
        colAc_edition.setCellValueFactory(new PropertyValueFactory<TableAcPe,String>("butTool"));

        table_account.setItems(listAccount);
    }

    private void tool_clicked(){
        tool_settins.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_settins,block_settins);
        });
        tool_account.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_account,block_account);
        });
        tool_blackLIst.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_blackLIst,block_blackList);
            try {
                Client.getInstance().getOut().writeObject(Request.UPDATE_BAN);
                initializedBanList();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        tool_statistic.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_statistic,block_statistic);
        });
        tool_report.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_report,block_report);
        });
        tool_computer.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_computer,block_computer);
        });
        tool_history.setOnMouseClicked(mouseEvent -> {
            tool_active(tool_history,block_history);
        });
        tool_exit.setOnMouseClicked(mouseEvent -> {
            tool_exit.getScene().getWindow().hide();
            try {
                Client.getInstance().getOut().writeObject(Request.EXIT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            AutorizationMenuController autorizationMenuController=new AutorizationMenuController();
            autorizationMenuController.Show();
        });
    }
    private void tool_active(HBox box,AnchorPane block){
        if(tool_settins.getStyleClass().contains("h-box_open"))tool_settins.getStyleClass().remove("h-box_open");
        if(tool_blackLIst.getStyleClass().contains("h-box_open"))tool_blackLIst.getStyleClass().remove("h-box_open");
        if(tool_history.getStyleClass().contains("h-box_open"))tool_history.getStyleClass().remove("h-box_open");
        if(tool_statistic.getStyleClass().contains("h-box_open"))tool_statistic.getStyleClass().remove("h-box_open");
        if(tool_report.getStyleClass().contains("h-box_open"))tool_report.getStyleClass().remove("h-box_open");
        if(tool_computer.getStyleClass().contains("h-box_open"))tool_computer.getStyleClass().remove("h-box_open");
        if(tool_account.getStyleClass().contains("h-box_open"))tool_account.getStyleClass().remove("h-box_open");
        block_account.setVisible(false);
        block_statistic.setVisible(false);
        block_report.setVisible(false);
        block_history.setVisible(false);
        block_computer.setVisible(false);
        block_settins.setVisible(false);
        block_blackList.setVisible(false);
        box.getStyleClass().add("h-box_open");
        block.setVisible(true);
    }
    private void initialized() throws IOException, ClassNotFoundException {
        initializedAccount();
        initializedTechnica();
        initlizedStatistic();
        initializedBanList();
    }
    @FXML
    void initialize() throws IOException, ClassNotFoundException {
        initializedComponent();
        initialized();
        tool_clicked();
        imp_searchTech.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                filterTech.setPredicate(myObject -> {
                    if (t1 == null || t1.isEmpty()) {
                        return true;
                    }
                    String lowerCaseFilter = t1.toLowerCase();
                    if (String.valueOf(myObject.getStringId()).toLowerCase().contains(lowerCaseFilter)) {
                        return true;
                    } else if (String.valueOf(myObject.getStringName()).toLowerCase().contains(lowerCaseFilter)) {
                        return true;
                    } else if (String.valueOf(myObject.getStringPrice()).toLowerCase().contains(lowerCaseFilter)) {
                        return true;
                    }
                    if (String.valueOf(myObject.getStringStatus()).toLowerCase().contains(lowerCaseFilter)) {
                        return true;
                    }
                    if (String.valueOf(myObject.getStringNumber()).toLowerCase().contains(lowerCaseFilter)) {
                        return true;
                    }
                    if (String.valueOf(myObject.getStringType()).toLowerCase().contains(lowerCaseFilter)) {
                        return true;
                    }
                        return false; // Does not match.
                });
            }
        });
        but_addTech.setOnAction(actionEvent -> {
            boolean trigger=false;
            ImageConfig imageConfig=new ImageConfig();
            FormValidator formValidator=new FormValidator();
            if(PrTypeReg.getValue()==null){trigger=true;formValidator.validatorComboBox(PrTypeReg);}
            if(PrNameReg.getText().equals("")){trigger=true;formValidator.validationInput(PrNameReg,"Поле не может быть пустым");}
            if(!PrPriceReg.getText().matches("^[-+]?[0-9]*[.][0-9]+(?:[eE][-+]?[0-9]+)?$")){trigger=true;formValidator.validationInput(PrPriceReg,"Введите стоимость");}
            if(trigger){
                formValidator.disabledComponent(but_addTech);
            }
            else {
                if(PrTypeReg.getValue()==PrTypeReg.getItems().get(0)){
                    boolean tr=false;
                    if(PrVideoCardReg.getText().equals("")){tr=true;formValidator.validationInput(PrVideoCardReg,"Поле не может быть пустым");}
                    if(PrCPUReg.getText().equals("")){tr=true;formValidator.validationInput(PrCPUReg,"Поле не может быть пустым");}
                    if(PrMNuberReg.getValue()==null){tr=true;formValidator.validatorComboBox(PrMNuberReg);}
                    if(PrPowerReg.getText().equals("")){tr=true;formValidator.validationInput(PrPowerReg,"Поле не может быть пустым");}
                    if(PrColotReg.getText().equals("")){tr=true;formValidator.validationInput(PrColotReg,"Поле не может быть пустым");}
                    if(tr){
                        formValidator.disabledComponent(but_addTech);
                    }
                    else {
                        try {
                            Technique technique=new Technique();
                            technique.setName(PrNameReg.getText());
                            technique.setNumber(numberTech.getValue());
                            technique.setPrice(Double.parseDouble(PrPriceReg.getText()));
                            technique.setType(PrTypeReg.getValue());
                            technique.setPhoto(imageConfig.SerializableImage(photoComputer));
                            Computer computer=new Computer();
                            computer.setPower_unit(PrPowerReg.getText());
                            computer.setVideo_card(PrVideoCardReg.getText());
                            computer.setCpu(PrCPUReg.getText());
                            computer.setMemory(PrMNuberReg.getValue());
                            computer.setColor(PrColotReg.getText());
                            Client.getInstance().getOut().writeObject(Request.ADD_PRODUCT);
                            Client.getInstance().getOut().writeObject(technique);
                            Client.getInstance().getOut().writeObject(computer);
                            formValidator.visableIcon(sec_addTech);
                            PrNameReg.setText("");
                            PrPriceReg.setText("");
                            PrPowerReg.setText("");
                            PrVideoCardReg.setText("");
                            PrCPUReg.setText("");
                            PrColotReg.setText("");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }                }
                if(PrTypeReg.getValue()==PrTypeReg.getItems().get(1)){
                    boolean tr=false;
                    if(PrMS_Video.getValue()==null){tr=true;formValidator.validatorComboBox(PrMS_Video);}
                    if(PrTMemory_Video.getText().equals("")){tr=true;formValidator.validationInput(PrTMemory_Video,"Поле не может быть пустым");}
                    if(!PrGPUF_Video.getText().matches("^\\d+$")){tr=false;formValidator.validationInput(PrGPUF_Video,"Частота это целое число");}
                    if(!PrGPUM_Video.getText().matches("^\\d+$")){tr=false;formValidator.validationInput(PrGPUM_Video,"Частота это целое число");}
                    if(tr){
                        formValidator.disabledComponent(but_addTech);
                    }
                    else {
                        try {
                            Technique technique=new Technique();
                            technique.setName(PrNameReg.getText());
                            technique.setNumber(numberTech.getValue());
                            technique.setPrice(Double.parseDouble(PrPriceReg.getText()));
                            technique.setType(PrTypeReg.getValue());
                            technique.setPhoto(imageConfig.SerializableImage(photoComputer));
                            VideoCard videoCard=new VideoCard();
                            videoCard.setFrequency_GPU(Double.parseDouble(PrGPUF_Video.getText()));
                            videoCard.setFrequency_memory(Double.parseDouble(PrGPUM_Video.getText()));
                            videoCard.setType_memory(PrTMemory_Video.getText());
                            videoCard.setMemory_size(PrMS_Video.getValue());
                            Client.getInstance().getOut().writeObject(Request.ADD_PRODUCT);
                            Client.getInstance().getOut().writeObject(technique);
                            Client.getInstance().getOut().writeObject(videoCard);
                            formValidator.visableIcon(sec_addTech);
                            PrNameReg.setText("");
                            PrPriceReg.setText("");
                            PrTMemory_Video.setText("");
                            PrGPUF_Video.setText("");
                            PrGPUM_Video.setText("");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(PrTypeReg.getValue()==PrTypeReg.getItems().get(2)){
                    boolean tr=false;
                    if(!RegCPUCF.getText().matches("^\\d+$")){tr=true; formValidator.validationInput(RegCPUCF,"Введите число !");}
                    if(!RegCPUWork.getText().matches("^\\d+$")){tr=true; formValidator.validationInput(RegCPUWork,"Введите кол-во FPS");}
                    if(!RegCPUEnergy.getText().matches("^[-+]?[0-9]*[.][0-9]+(?:[eE][-+]?[0-9]+)?$")){tr=true;formValidator.validationInput(RegCPUEnergy,"Введите Ват/час");}
                    if(tr){
                        formValidator.disabledComponent(but_addTech);
                    }
                    else {
                        try {
                            Technique technique=new Technique();
                            technique.setName(PrNameReg.getText());
                            technique.setNumber(numberTech.getValue());
                            technique.setPrice(Double.parseDouble(PrPriceReg.getText()));
                            technique.setType(PrTypeReg.getValue());
                            technique.setPhoto(imageConfig.SerializableImage(photoComputer));
                            Cpu cpu=new Cpu();
                            cpu.setClock_frequency(Double.parseDouble(RegCPUCF.getText()));
                            cpu.setEnergy_consumption(Double.parseDouble(RegCPUEnergy.getText()));
                            cpu.setNumber_cores(RegCPUCore.getValue());
                            cpu.setPerformance(Double.parseDouble(RegCPUWork.getText()));
                            Client.getInstance().getOut().writeObject(Request.ADD_PRODUCT);
                            Client.getInstance().getOut().writeObject(technique);
                            Client.getInstance().getOut().writeObject(cpu);
                            formValidator.visableIcon(sec_addTech);
                            RegCPUCF.setText("");
                            RegCPUWork.setText("");
                            RegCPUEnergy.setText("");
                            PrPriceReg.setText("");
                            PrNameReg.setText("");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(PrTypeReg.getValue()==PrTypeReg.getItems().get(3)){
                    boolean tr=false;
                    if(!PrNumberVolt.getText().matches("^\\d+$")){tr=true;formValidator.validationInput(PrNumberVolt,"Введите целое число");}
                    if(PrMarkP.getText().equals("")){tr=true;formValidator.validationInput(PrMarkP,"Поле не может быть пустым");}
                    if(tr){
                        formValidator.disabledComponent(but_addTech);
                    }
                    else {
                        try {
                            Technique technique=new Technique();
                            technique.setName(PrNameReg.getText());
                            technique.setNumber(numberTech.getValue());
                            technique.setPrice(Double.parseDouble(PrPriceReg.getText()));
                            technique.setType(PrTypeReg.getValue());
                            technique.setPhoto(imageConfig.SerializableImage(photoComputer));
                            PowerUnit powerUnit=new PowerUnit();
                            powerUnit.setMark(PrMarkP.getText());
                            powerUnit.setNumber_volts(Integer.parseInt(PrNumberVolt.getText()));
                            Client.getInstance().getOut().writeObject(Request.ADD_PRODUCT);
                            Client.getInstance().getOut().writeObject(technique);
                            Client.getInstance().getOut().writeObject(powerUnit);
                            formValidator.visableIcon(sec_addTech);
                            PrNumberVolt.setText("");
                            PrMarkP.setText("");
                            PrPriceReg.setText("");
                            PrNameReg.setText("");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(PrTypeReg.getValue()==PrTypeReg.getItems().get(4)){}
            }
        });
        PrTypeReg.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                if(PrTypeReg.getItems().get((Integer)t1).equals(PrTypeReg.getItems().get(0))){
                    box_computer.setVisible(true);
                    box_video.setVisible(false);
                    box_processor.setVisible(false);
                    box_blockUp.setVisible(false);
                }
                if(PrTypeReg.getItems().get((Integer)t1).equals(PrTypeReg.getItems().get(1))){
                    box_computer.setVisible(false);
                    box_processor.setVisible(false);
                    box_blockUp.setVisible(false);
                    box_video.setVisible(true);
                }
                if(PrTypeReg.getItems().get((Integer)t1).equals(PrTypeReg.getItems().get(2))){
                    box_computer.setVisible(false);
                    box_processor.setVisible(true);
                    box_blockUp.setVisible(false);
                    box_video.setVisible(false);
                }
                if(PrTypeReg.getItems().get((Integer)t1).equals(PrTypeReg.getItems().get(3))){
                    box_computer.setVisible(false);
                    box_processor.setVisible(false);
                    box_blockUp.setVisible(true);
                    box_video.setVisible(false);
                }
                if(PrTypeReg.getItems().get((Integer)t1).equals(PrTypeReg.getItems().get(4))){
                    box_computer.setVisible(false);
                    box_processor.setVisible(false);
                    box_blockUp.setVisible(false);
                    box_video.setVisible(false);
                }
            }
        });
        tableTech.setRowFactory(tableTeqTableView -> {
            TableRow<TableTeq> row=new TableRow<>();
            row.setOnMouseClicked(mouseEvent -> {
                if(mouseEvent.getClickCount()==2&&(!row.isEmpty())){
                    try {
                        Client.getInstance().getOut().writeObject(Request.PERSONAL_TECH);
                        Client.getInstance().getOut().writeObject(tableTech.getSelectionModel().getSelectedItem().getStringId());
                        PersonalTechniqueController personalTechniqueController=new PersonalTechniqueController();
                        personalTechniqueController.Show();
                        Client.getInstance().getOut().writeObject(Request.UPDATE_TECH);
                        initializedTechnica();
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
            return row;
        });
        setB_save.setOnAction(actionEvent -> {
            boolean trigger=false;
            FormValidator formValidator=new FormValidator();
            if(!set_name.getText().matches("[ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэюбьтимсчяёЁ]{2,40}")){formValidator.validationInput(set_name,"Имя не может сожержать цифры");trigger=true;}
            if(!set_surName.getText().matches("[ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэюбьтимсчяёЁ]{2,40}")){formValidator.validationInput(set_surName,"Фамилия не может сожержать цифры");trigger=true;}
            if(!set_patronymic.getText().matches("[ЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТЬБЮйцукенгшщзхъфывапролджэюбьтимсчяёЁ]{2,40}")){formValidator.validationInput(set_patronymic,"Отчество не может сожержать цифры");trigger=true;}
            if(set_password.getText().equals("")||set_password.getText().length()<2){formValidator.validationInput(set_password,"Пароль должен быть более 2 символов");trigger=true;}
            if(set_login.getText().equals("")||set_login.getText().length()<2){formValidator.validationInput(set_login,"Логин должен быть более 2 символов");trigger=true;}
            if(!set_phone.getText().matches("(\\+375|80)\\d{9}")){
                formValidator.validationInput(set_phone,"+375 или 80 29,44,25.. xxxxxxx");trigger=true;}
            if(trigger){
                formValidator.disabledComponent(setB_save);
            }
            else {
                try {
                    ImageConfig imageConfig=new ImageConfig();
                    Account account=new Account();
                    account.setPassword(set_password.getText());
                    account.setLogin(set_login.getText());
                    if(photo!=null){
                        account.setPhoto(imageConfig.SerializableImage(photo));
                    }
                    Person person = new Person();
                    person.setCountry("Минск");
                    person.setName(set_name.getText());
                    person.setSur_name(set_surName.getText());
                    person.setPatronymic(set_patronymic.getText());
                    person.setPhone(set_phone.getText());
                    Client.getInstance().getOut().writeObject(Request.SAVE_PARSONAL_INFO);
                    Client.getInstance().getOut().writeObject(account);
                    Client.getInstance().getOut().writeObject(person);
                    formValidator.visableIcon(setI_sec);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        setB_downloadPhoto.setOnAction(actionEvent -> {
            FileChooser fileChooser=new FileChooser();
            fileChooser.setTitle("Выберите фото");
            FileChooser.ExtensionFilter filter=new FileChooser.ExtensionFilter("Select ...",".png",".jpg");
            fileChooser.getExtensionFilters()
                    .addAll(new FileChooser.ExtensionFilter("Select ...","*.png","*.jpg"));
            photo=fileChooser.showOpenDialog(stage);
            if(photo!=null){
                Image image=new Image("file:///"+photo.getAbsolutePath());
                setPhoto.setImage(image);
            }
        });
        but_downloadComp.setOnAction(actionEvent -> {
            FileChooser fileChooser=new FileChooser();
            fileChooser.setTitle("Выберите фото");
            FileChooser.ExtensionFilter filter=new FileChooser.ExtensionFilter("Select ...",".png",".jpg");
            fileChooser.getExtensionFilters()
                    .addAll(new FileChooser.ExtensionFilter("Select ...","*.png","*.jpg"));
            photoComputer=fileChooser.showOpenDialog(stage);
            if(photoComputer!=null){
                Image image=new Image("file:///"+photoComputer.getAbsolutePath());
                photoComp.setImage(image);
            }
        });
        table_account.setRowFactory(tableAcPeTableView -> {
            TableRow<TableAcPe>row=new TableRow<>();
            row.setOnMouseClicked(mouseEvent -> {
                if(mouseEvent.getClickCount()==2&&(!row.isEmpty())){
                    try {
                        Client.getInstance().getOut().writeObject(Request.PERSONAL_ACCOUNT);
                        Label label=(Label)tableAcPeTableView.getSelectionModel().getSelectedItem().getId_account().getChildren().get(0);
                        Client.getInstance().getOut().writeObject(Integer.parseInt(label.getText()));
                        PersonalAccountController personalAccountController=new PersonalAccountController();
                        personalAccountController.Show();
                        Client.getInstance().getOut().writeObject(Request.UPDATE_ACCOUNT);
                        initializedAccount();
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
            return row;
        });
        menu_bar.setOnMouseClicked(mouseEvent -> {
            if(menu_bar.getGlyphName().equals("ALIGN_JUSTIFY")){
                menu_bar.setIcon(FontAwesomeIcon.ALIGN_LEFT);
                tools_bar.getStyleClass().remove("v-box");
                tools_bar.getStyleClass().add("v-box-open");

            }
            else {
                menu_bar.setIcon(FontAwesomeIcon.ALIGN_JUSTIFY);
                tools_bar.getStyleClass().remove("v-box-open");
                tools_bar.getStyleClass().add("v-box");
            }
        });
        tougleRegComp.setOnMouseClicked(mouseEvent -> {
            if(!tougleRegComp.getStyleClass().contains("toggle-button_close")){
                tougleRegComp.getStyleClass().add("toggle-button_close");
                tougleListComp.getStyleClass().remove("toggle-button_close");
                secBlock_list_technica.setVisible(false);
                secBlock_reg_technica.setVisible(true);
            }
        });
        tougleListComp.setOnMouseClicked(mouseEvent -> {
            if(!tougleListComp.getStyleClass().contains("toggle-button_close")){
                tougleListComp.getStyleClass().add("toggle-button_close");
                tougleRegComp.getStyleClass().remove("toggle-button_close");
                secBlock_reg_technica.setVisible(false);
                secBlock_list_technica.setVisible(true);
            }
        });
    }

}
