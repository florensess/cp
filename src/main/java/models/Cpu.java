package models;

import java.io.Serializable;

public class Cpu implements Serializable {
    int id;
    int id_technique;
    double clock_frequency;
    double performance;
    double energy_consumption;
    int number_cores;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_technique() {
        return id_technique;
    }

    public void setId_technique(int id_technique) {
        this.id_technique = id_technique;
    }

    public double getClock_frequency() {
        return clock_frequency;
    }

    public void setClock_frequency(double clock_frequency) {
        this.clock_frequency = clock_frequency;
    }

    public double getPerformance() {
        return performance;
    }

    public void setPerformance(double performance) {
        this.performance = performance;
    }

    public double getEnergy_consumption() {
        return energy_consumption;
    }

    public void setEnergy_consumption(double energy_consumption) {
        this.energy_consumption = energy_consumption;
    }

    public int getNumber_cores() {
        return number_cores;
    }

    public void setNumber_cores(int number_cores) {
        this.number_cores = number_cores;
    }
}
