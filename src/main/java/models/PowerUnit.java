package models;

import java.io.Serializable;

public class PowerUnit implements Serializable {
    int id;
    int id_technique;
    int number_volts;
    String mark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_technique() {
        return id_technique;
    }

    public void setId_technique(int id_technique) {
        this.id_technique = id_technique;
    }

    public int getNumber_volts() {
        return number_volts;
    }

    public void setNumber_volts(int number_volts) {
        this.number_volts = number_volts;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
