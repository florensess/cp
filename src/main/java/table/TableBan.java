package table;

import client.Client;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import models.Account;
import models.BanList;
import mvc.Request;

import java.io.IOException;

public class TableBan {
    HBox id;
    HBox photo;
    HBox login;
    HBox status;
    HBox reason;
    HBox date;
    HBox edition;


    public void setId(int id) {
        this.id=new HBox();
        this.id.setMaxHeight(40);
        this.id.setMaxHeight(40);
        this.id.setPrefHeight(40);
        this.id.setPrefWidth(40);
        this.id.setAlignment(Pos.CENTER);
        Label label=new Label(Integer.toString(id));
        this.id.getChildren().add(label);
    }

    public void setDate(String date) {
        this.date=new HBox();
        this.date.setMaxHeight(40);
        this.date.setMaxHeight(40);
        this.date.setPrefHeight(40);
        this.date.setPrefWidth(40);
        this.date.setAlignment(Pos.CENTER);
        Label label=new Label(date);
        this.date.getChildren().add(label);
    }

    public void setPhoto(Image image){
        photo=new HBox();
        photo.setMaxHeight(40);
        photo.setMaxHeight(40);
        photo.setPrefHeight(40);
        photo.setPrefWidth(40);
        photo.setAlignment(Pos.CENTER);
        ImageView imageView=new ImageView(image);
        imageView.setFitWidth(40);
        imageView.setFitHeight(40);
        photo.getChildren().add(imageView);
    }

    public void setStatus(String status) {
        this.status=new HBox();
        this.status.setMaxHeight(40);
        this.status.setMaxHeight(40);
        this.status.setPrefHeight(40);
        this.status.setPrefWidth(40);
        this.status.setAlignment(Pos.CENTER);
        Label label=new Label(status);
        this.status.getChildren().add(label);
    }

    public void setLogin(String login){
        this.login=new HBox();
        this.login.setMaxHeight(40);
        this.login.setMaxHeight(40);
        this.login.setPrefHeight(40);
        this.login.setPrefWidth(40);
        this.login.setAlignment(Pos.CENTER);
        Label label=new Label(login);
        this.login.getChildren().add(label);
    }

    public void setEdition(){
        this.edition=new HBox();
        this.edition.setMaxHeight(40);
        this.edition.setMaxHeight(40);
        this.edition.setPrefHeight(40);
        this.edition.setPrefWidth(40);
        this.edition.setAlignment(Pos.CENTER);
        FontAwesomeIconView deleteIcon = new FontAwesomeIconView(FontAwesomeIcon.UNLOCK);
        deleteIcon.setSize("28px");
        deleteIcon.setFill(Paint.valueOf("red"));
        deleteIcon.setCursor(Cursor.HAND);
        deleteIcon.setOnMouseClicked(mouseEvent -> {
            try {
                Client.getInstance().getOut().writeObject(Request.UNBLOCK_ACCOUNT);
                TableView<TableAcPe> tableAcPeTableView=(TableView<TableAcPe>)deleteIcon.getParent().getParent().getParent().getParent().getParent().getParent().getParent();
                Label label=(Label)tableAcPeTableView.getSelectionModel().getSelectedItem().getPhone().getChildren().get(0);
                tableAcPeTableView.getItems().remove(tableAcPeTableView.getSelectionModel().getSelectedItem());
                Client.getInstance().getOut().writeObject(Integer.parseInt(label.getText()));
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        this.edition.getChildren().add(deleteIcon);
    }

    public void setReason(String reason) {
        this.reason=new HBox();
        this.reason.setMaxHeight(40);
        this.reason.setMaxHeight(40);
        this.reason.setPrefHeight(40);
        this.reason.setPrefWidth(40);
        this.reason.setAlignment(Pos.CENTER);
        TextArea label=new TextArea(reason);
        label.setPrefWidth(200);
        label.setPrefHeight(40);
        this.reason.getChildren().add(label);
    }

    public TableBan(BanList banLis, Account account) throws IOException, ClassNotFoundException {
        ImageConfig imageConfig=new ImageConfig();
        setDate(banLis.getDate());
        setLogin(account.getLogin());
        setStatus(account.getStatus());
        setId(account.getId());
        setEdition();
        setReason(banLis.getReason());
        setPhoto(imageConfig.DeserializableImage(account.getPhoto()));
    }

    public HBox getId() {
        return id;
    }

    public HBox getPhoto() {
        return photo;
    }

    public HBox getLogin() {
        return login;
    }

    public HBox getStatus() {
        return status;
    }

    public HBox getReason() {
        return reason;
    }

    public HBox getDate() {
        return date;
    }

    public HBox getEdition() {
        return edition;
    }
}
