package table;

import client.Client;
import config.ImageConfig;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import models.Account;
import models.Person;
import mvc.AccountStatus;
import mvc.Request;

import javax.swing.text.TabableView;
import java.io.IOException;
import java.util.List;
import java.awt.*;

public class TableAcPe {
    public TableAcPe(Person person,Account account) throws IOException, ClassNotFoundException {
            id_account=new HBox();
            id_account.setAlignment(Pos.CENTER);
            date_registaration.setAlignment(Pos.CENTER);
            role.setAlignment(Pos.CENTER);
        ImageConfig imageConfig=new ImageConfig();
            setDate_registaration(account.getDate_registration());
            setStatus(account.getStatus());
            setLogin(account.getLogin());
            setPhone(person.getPhone());
            setRole(account.getRole());
            setId_account(account.getId());
            setPhoto(imageConfig.DeserializableImage(account.getPhoto()));
            if(account.getRole().equals(AccountStatus.user)){
                this.butTool=new HBox();
                this.butTool.setMaxHeight(40);
                this.butTool.setMaxHeight(40);
                this.butTool.setPrefHeight(40);
                this.butTool.setPrefWidth(40);
                this.butTool.setAlignment(Pos.CENTER);
                FontAwesomeIconView deleteIcon = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                deleteIcon.setSize("28px");
                deleteIcon.setFill(Paint.valueOf("red"));
                deleteIcon.setCursor(Cursor.HAND);
                deleteIcon.setOnMouseClicked(mouseEvent -> {
                    try {
                        Client.getInstance().getOut().writeObject(Request.REMOVE_ACCOUNT);
                        TableView<TableAcPe>tableAcPeTableView=(TableView<TableAcPe>)deleteIcon.getParent().getParent().getParent().getParent().getParent().getParent().getParent();
                        Label label=(Label)tableAcPeTableView.getSelectionModel().getSelectedItem().getPhone().getChildren().get(0);
                        tableAcPeTableView.getItems().remove(tableAcPeTableView.getSelectionModel().getSelectedItem());
                        Client.getInstance().getOut().writeObject(Integer.parseInt(label.getText()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });
                this.butTool.getChildren().add(deleteIcon);
            }
    }

    public HBox getId_account() {
        return id_account;
    }

    public HBox getLogin() {
        return login;
    }

    public HBox getPhone() {
        return phone;
    }

    public HBox getStatus() {
        return status;
    }

    public HBox getRole() {
        return role;
    }

    public HBox getDate_registaration() {
        return date_registaration;
    }

    public void setId_account(int id_account) {
        this.id_account=new HBox();
        this.id_account.setMaxHeight(40);
        this.id_account.setMaxHeight(40);
        this.id_account.setPrefHeight(40);
        this.id_account.setPrefWidth(40);
        this.id_account.setAlignment(Pos.CENTER);
        Label label=new Label(Integer.toString(id_account));
        this.id_account.getChildren().add(label);
    }


    public void setLogin(String login) {
        this.login=new HBox();
        this.login.setMaxHeight(40);
        this.login.setMaxHeight(40);
        this.login.setPrefHeight(40);
        this.login.setPrefWidth(40);
        this.login.setAlignment(Pos.CENTER);
        Label label=new Label(login);
        this.login.getChildren().add(label);
    }


    public void setPhone(String phone) {
        this.phone=new HBox();
        this.phone.setMaxHeight(40);
        this.phone.setMaxHeight(40);
        this.phone.setPrefHeight(40);
        this.phone.setPrefWidth(40);
        this.phone.setAlignment(Pos.CENTER);
        Label label=new Label(phone);
        this.phone.getChildren().add(label);
    }



    public void setStatus(String status) {
        this.status=new HBox();
        this.status.setMaxHeight(40);
        this.status.setMaxHeight(40);
        this.status.setPrefHeight(40);
        this.status.setPrefWidth(40);
        this.status.setAlignment(Pos.CENTER);
        Label label=new Label(status);
        this.status.getChildren().add(label);
    }


    public void setRole(String rol) {

        role=new HBox();
        role.setMaxHeight(40);
        role.setMaxHeight(40);
        role.setPrefHeight(40);
        role.setPrefWidth(40);
        role.setAlignment(Pos.CENTER);
        Label label=new Label(rol);
        role.getChildren().add(label);
    }


    public void setDate_registaration(String date) {
        date_registaration=new HBox();
        date_registaration.setMaxHeight(40);
        date_registaration.setMaxHeight(40);
        date_registaration.setPrefHeight(40);
        date_registaration.setPrefWidth(40);
        date_registaration.setAlignment(Pos.CENTER);
        Label label=new Label(date);
        date_registaration.getChildren().add(label);
    }

    public HBox getButTool() {
        return butTool;
    }


    HBox butTool=new HBox();
    HBox id_account=new HBox();
    HBox login=new HBox();
    HBox phone=new HBox();
    HBox photo=new HBox();
    HBox status=new HBox();
    HBox role=new HBox();
    HBox date_registaration=new HBox();
   public HBox getPhoto(){
       return photo;}
  public  void setPhoto(Image image){
        photo=new HBox();
        photo.setMaxHeight(40);
        photo.setMaxHeight(40);
        photo.setPrefHeight(40);
        photo.setPrefWidth(40);
        photo.setAlignment(Pos.CENTER);
        ImageView imageView=new ImageView(image);
        imageView.setFitWidth(40);
        imageView.setFitHeight(40);
        photo.getChildren().add(imageView);
    }
}
