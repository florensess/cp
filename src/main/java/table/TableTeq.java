package table;

import config.ImageConfig;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import models.Technique;

import java.io.IOException;

public class TableTeq {
    HBox id;
    HBox photo;

    public HBox getId() {
        return id;
    }

    public HBox getPhoto() {
        return photo;
    }

    public HBox getName() {
        return name;
    }

    public HBox getStatus() {
        return status;
    }

    public HBox getPrice() {
        return price;
    }

    public HBox getNumber() {
        return number;
    }

    public HBox getType() {
        return type;
    }

    public int getStringId(){
        Label label=(Label) id.getChildren().get(0);
        return Integer.parseInt(label.getText());
    }
    public String getStringName(){
        Label label=(Label) name.getChildren().get(0);
        return label.getText();
    }

    public String getStringStatus(){
        Label label=(Label) status.getChildren().get(0);
        return label.getText();
    }

    public double getStringPrice(){
        Label label=(Label) price.getChildren().get(0);
        return Double.parseDouble(label.getText());
    }

    public int getStringNumber(){
        Label label=(Label) number.getChildren().get(0);
        return Integer.parseInt(label.getText());
    }

    public String getStringType(){
        Label label=(Label) type.getChildren().get(0);
        return label.getText();
    }

    HBox name;
    HBox status;
    HBox price;
    HBox number;
    HBox type;

    public void setId(int id) {
        this.id=new HBox();
        this.id.setMaxHeight(40);
        this.id.setMaxHeight(40);
        this.id.setPrefHeight(40);
        this.id.setPrefWidth(40);
        this.id.setAlignment(Pos.CENTER);
        Label label=new Label(Integer.toString(id));
        this.id.getChildren().add(label);
    }

    public void setPhoto(Image image) {
        photo=new HBox();
        photo.setMaxHeight(40);
        photo.setMaxHeight(40);
        photo.setPrefHeight(40);
        photo.setPrefWidth(40);
        photo.setAlignment(Pos.CENTER);
        ImageView imageView=new ImageView(image);
        imageView.setFitWidth(40);
        imageView.setFitHeight(40);
        photo.getChildren().add(imageView);
    }

    public void setName(String name) {
        this.name=new HBox();
        this.name.setMaxHeight(40);
        this.name.setMaxHeight(40);
        this.name.setPrefHeight(40);
        this.name.setPrefWidth(40);
        this.name.setAlignment(Pos.CENTER);
        Label label=new Label(name);
        this.name.getChildren().add(label);
    }

    public void setStatus(String status) {
        this.status=new HBox();
        this.status.setMaxHeight(40);
        this.status.setMaxHeight(40);
        this.status.setPrefHeight(40);
        this.status.setPrefWidth(40);
        this.status.setAlignment(Pos.CENTER);
        Label label=new Label(status);
        this.status.getChildren().add(label);
    }

    public void setPrice(Double price) {
        this.price=new HBox();
        this.price.setMaxHeight(40);
        this.price.setMaxHeight(40);
        this.price.setPrefHeight(40);
        this.price.setPrefWidth(40);
        this.price.setAlignment(Pos.CENTER);
        Label label=new Label(Double.toString(price));
        this.price.getChildren().add(label);
    }

    public void setNumber(int number) {
        this.number=new HBox();
        this.number.setMaxHeight(40);
        this.number.setMaxHeight(40);
        this.number.setPrefHeight(40);
        this.number.setPrefWidth(40);
        this.number.setAlignment(Pos.CENTER);
        Label label=new Label(Integer.toString(number));
        this.number.getChildren().add(label);
    }

    public void setType(String type) {
        this.type=new HBox();
        this.type.setMaxHeight(40);
        this.type.setMaxHeight(40);
        this.type.setPrefHeight(40);
        this.type.setPrefWidth(40);
        this.type.setAlignment(Pos.CENTER);
        Label label=new Label(type);
        this.type.getChildren().add(label);
    }

    public TableTeq(Technique technique) throws IOException, ClassNotFoundException {
        ImageConfig imageConfig=new ImageConfig();
        setNumber(technique.getNumber());
        setPhoto(imageConfig.DeserializableImage(technique.getPhoto()));
        setPrice(technique.getPrice());
        setStatus(technique.getStatus());
        setType(technique.getType());
        setId(technique.getId());
        setName(technique.getName());
    }
}
