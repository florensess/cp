package config;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextInputControl;

import javax.sound.sampled.Control;
import java.awt.*;

public class FormValidator {
    public void validatorComboBox(ComboBox<String> comboBox){
        comboBox.setStyle("-fx-text-fill: #9a0202; -fx-border-color: #9a0202");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    comboBox.setStyle("");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void visableIcon(FontAwesomeIconView fontAwesomeIconView){
        fontAwesomeIconView.setVisible(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    fontAwesomeIconView.setVisible(false);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void viseableNode(Node node){
        node.setVisible(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    node.setVisible(false);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void ArletMessage(Label label,String message){
        label.setText(message);
        label.setVisible(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    label.setVisible(false);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }
    public void disabledComponent(Node node){
        node.setDisable(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    node.setDisable(false);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void validationInput(TextInputControl node, String messages){
        String oldText=node.getText();
        node.setText(messages);
        node.setEditable(false);
        node.setStyle("-fx-text-fill: #9a0202; -fx-border-color: #9a0202;");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    node.setText(oldText);
                    node.setStyle("");
                    node.setEditable(true);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
