package models;

import java.io.Serializable;

public class VideoCard implements Serializable {
    int id;
    int id_technique;
    String memory_size;
    String type_memory;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_technique() {
        return id_technique;
    }

    public void setId_technique(int id_technique) {
        this.id_technique = id_technique;
    }

    public String getMemory_size() {
        return memory_size;
    }

    public void setMemory_size(String memory_size) {
        this.memory_size = memory_size;
    }

    public String getType_memory() {
        return type_memory;
    }

    public void setType_memory(String type_memory) {
        this.type_memory = type_memory;
    }

    public double getFrequency_memory() {
        return frequency_memory;
    }

    public void setFrequency_memory(double frequency_memory) {
        this.frequency_memory = frequency_memory;
    }

    public double getFrequency_GPU() {
        return frequency_GPU;
    }

    public void setFrequency_GPU(double frequency_GPU) {
        this.frequency_GPU = frequency_GPU;
    }

    double frequency_memory;
    double frequency_GPU;
}
