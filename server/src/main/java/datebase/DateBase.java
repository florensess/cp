package datebase;

import models.*;
import mvc.AccountStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DateBase extends DateBaseConfig {
    private static Connection getDbconnection()throws ClassNotFoundException, SQLException {
        Connection dbconnection;
        String ConectionString ="jdbc:mysql://"+dbHost+":"+
                dbport+"/"+dbName+"?serverTimezone=Europe/Moscow";
        Class.forName("com.mysql.cj.jdbc.Driver");
        System.out.println(ConectionString);
        dbconnection= DriverManager.getConnection(ConectionString, dbUsers,dbPass);
        return dbconnection;
    }
    public void updateAccountInfo(Account account,Person person) throws SQLException, ClassNotFoundException {
        String query="UPDATE computer.account set computer.account.password = '"+account.getPassword()+
                "' , computer.account.login = '"+account.getLogin()+"' , computer.account.photo = '"+account.getPhoto()+
                "' where computer.account.id = "+account.getId();
        PreparedStatement statement=getDbconnection().prepareStatement(query);
        statement.executeUpdate();
        String queryPatient="UPDATE computer.person set computer.person.name = '"+person.getName()+"' , computer.person.sur_name = '"+
                person.getSur_name()+"' , computer.person.patronymic = '"+person.getPatronymic()+"' , computer.person.phone = '"+
                person.getPhone()+"' where person.id_account = "+person.getId_account();
        PreparedStatement statementPatient=getDbconnection().prepareStatement(queryPatient);

        statementPatient.executeUpdate();
    }
    public Account autorizationRequest(Account account) throws SQLException, ClassNotFoundException {
        String find_user="SELECT * FROM computer.account  WHERE account.login = "+"'"+account.getLogin()+"'"+" AND account.password = "+"'"+
                account.getPassword()+"'";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_user);
        ResultSet res = preparedStatement.executeQuery();
        Account user=new Account();
        while (res.next()){
            user.setId(res.getInt("id"));
            user.setStatus(res.getString("status"));
            user.setRole(res.getString("role"));
            user.setPhoto(res.getString("photo"));
            user.setDate_registration(res.getString("date_registration"));
            user.setLogin(res.getString("login"));
            user.setPassword(res.getString("password"));
        }
        return user;
    }
    public void removeRecodsBanList(int id_account) throws SQLException, ClassNotFoundException {
        String query="DELETE FROM computer.ban_list WHERE computer.ban_list.id_account="+id_account;
        PreparedStatement statement =getDbconnection().prepareStatement(query);
        statement.executeUpdate(query);
    }
    public void removeAccount(int id) throws SQLException, ClassNotFoundException {
        String query="DELETE FROM computer.account WHERE account.id="+id;
        PreparedStatement statement =getDbconnection().prepareStatement(query);
        statement.executeUpdate(query);
    }
    public void removeProduct(int id) throws SQLException, ClassNotFoundException {
        String query="DELETE FROM computer.technique WHERE technique.id="+id;
        PreparedStatement statement =getDbconnection().prepareStatement(query);
        statement.executeUpdate(query);
    }
    public Account findAccountById(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.account WHERE computer.account.id = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        Account account=new Account();
        while (res.next()){
            account.setId(res.getInt("id"));
            account.setStatus(res.getString("status"));
            account.setRole(res.getString("role"));
            account.setPhoto(res.getString("photo"));
            account.setDate_registration(res.getString("date_registration"));
            account.setLogin(res.getString("login"));
            account.setPassword(res.getString("password"));
        }
        return account;
    }
    public List<Account> findAllBLockAccount( String accountStatus) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.account WHERE  computer.account.status='"
                +accountStatus+"'";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        List<Account>list=new ArrayList<>();
        while (res.next()){
            Account account=new Account();
            account.setId(res.getInt("id"));
            account.setStatus(res.getString("status"));
            account.setRole(res.getString("role"));
            account.setPhoto(res.getString("photo"));
            account.setDate_registration(res.getString("date_registration"));
            account.setLogin(res.getString("login"));
            account.setPassword(res.getString("password"));
            list.add(account);
        }
        return list;
    }
    public List<Account> findAllAccountByRole(String role) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.account WHERE  computer.account.role='"
                +role+"'";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        List<Account>list=new ArrayList<>();
        while (res.next()){
            Account account=new Account();
            account.setId(res.getInt("id"));
            account.setStatus(res.getString("status"));
            account.setRole(res.getString("role"));
            account.setPhoto(res.getString("photo"));
            account.setDate_registration(res.getString("date_registration"));
            account.setLogin(res.getString("login"));
            account.setPassword(res.getString("password"));
            list.add(account);
        }
        return list;
    }
    public Technique findTechniqueById(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.technique WHERE computer.technique.id = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        Technique technique=new Technique();
        while (res.next()){
            technique.setId(res.getInt("id"));
            technique.setDate_reg(res.getString("date_take"));
            technique.setPhoto(res.getString("photo"));
            technique.setStatus(res.getString("status"));
            technique.setName(res.getString("name_technique"));
            technique.setType(res.getString("type"));
            technique.setPrice(res.getDouble("price"));
            technique.setNumber(res.getInt("number"));
        }
        return technique;
    }
    public Computer findComputerByIdTechnique(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.computer WHERE computer.computer.id_technique = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        Computer computer=new Computer();
        while (res.next()){
            computer.setId(res.getInt("id"));
            computer.setId_technique(res.getInt("id_technique"));
            computer.setVideo_card(res.getString("video_card"));
            computer.setCpu(res.getString("processor"));
            computer.setMemory(res.getString("memory"));
            computer.setPower_unit(res.getString("power_unit"));
            computer.setColor(res.getString("color"));
        }
        return computer;
    }
    public VideoCard findVideoCardByIdTechnique(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.video_card WHERE computer.video_card.id_technique = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        VideoCard videoCard=new VideoCard();
        while (res.next()){
            videoCard.setId(res.getInt("id"));
            videoCard.setId_technique(res.getInt("id_technique"));
            videoCard.setMemory_size(res.getString("memory_size"));
            videoCard.setFrequency_memory(res.getDouble("frequency_memory"));
            videoCard.setFrequency_GPU(res.getDouble("frequency_GPU"));
            videoCard.setType_memory(res.getString("type_memory"));
        }
        return videoCard;
    }
    public Cpu findCPUByIdTechnique(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.cpu WHERE computer.cpu.id_technique = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        Cpu cpu=new Cpu();
        while (res.next()){
            cpu.setId(res.getInt("id"));
            cpu.setId_technique(res.getInt("id_technique"));
            cpu.setPerformance(res.getDouble("performance"));
            cpu.setEnergy_consumption(res.getDouble("clock_frequency"));
            cpu.setClock_frequency(res.getDouble("energy_consumption"));
            cpu.setNumber_cores(res.getInt("number_cores"));
        }
        return cpu;
    }
    public PowerUnit findPowerByIdTechnique(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.power_unit WHERE computer.power_unit.id_technique = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        PowerUnit powerUnit=new PowerUnit();
        while (res.next()){
            powerUnit.setId(res.getInt("id"));
            powerUnit.setId_technique(res.getInt("id_technique"));
            powerUnit.setNumber_volts(res.getInt("number_volts"));
            powerUnit.setMark(res.getString("mark"));
        }
        return powerUnit;
    }
    public Person findPersonById(int id) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.person WHERE computer.person.id = "+id;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        Person person=new Person();
        while (res.next()){
            person.setId(res.getInt("id"));
            person.setId_account(res.getInt("id_account"));
            person.setName(res.getString("name"));
            person.setSur_name(res.getString("sur_name"));
            person.setPatronymic(res.getString("patronymic"));
            person.setPhone(res.getString("phone"));
            person.setCountry(res.getString("country"));
        }
        return person;
    }
    public Person findPersonByIdAccount(int id_account) throws SQLException, ClassNotFoundException {
        String find_account="SELECT * FROM computer.person WHERE computer.person.id_account = "+id_account;
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_account);
        ResultSet res = preparedStatement.executeQuery();
        Person person=new Person();
        while (res.next()){
            person.setId(res.getInt("id"));
            person.setId_account(res.getInt("id_account"));
            person.setName(res.getString("name"));
            person.setSur_name(res.getString("sur_name"));
            person.setPatronymic(res.getString("patronymic"));
            person.setPhone(res.getString("phone"));
            person.setCountry(res.getString("country"));
        }
        return person;
    }
    public List<Person>getAllPerson() throws SQLException, ClassNotFoundException {
        String find_user="SELECT * FROM computer.person";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_user);
        ResultSet res = preparedStatement.executeQuery();
        List<Person>list=new ArrayList<>();
        while (res.next()){
            Person person=new Person();
            person.setId(res.getInt("id"));
            person.setId_account(res.getInt("id_account"));
            person.setName(res.getString("name"));
            person.setSur_name(res.getString("sur_name"));
            person.setPatronymic(res.getString("patronymic"));
            person.setPhone(res.getString("phone"));
            person.setCountry(res.getString("country"));
            list.add(person);
        }
        return list;
    }
    public List<Account>getAllAccount() throws SQLException, ClassNotFoundException {
        String find_user="SELECT * FROM computer.account";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_user);
        ResultSet res = preparedStatement.executeQuery();
        List<Account>list=new ArrayList<>();
        while (res.next()){
            Account user=new Account();
            user.setId(res.getInt("id"));
            user.setStatus(res.getString("status"));
            user.setRole(res.getString("role"));
            user.setPhoto(res.getString("photo"));
            user.setDate_registration(res.getString("date_registration"));
            user.setLogin(res.getString("login"));
            user.setPassword(res.getString("password"));
            list.add(user);
        }
        return list;
    }
    public List<BanList>getAllBanList() throws SQLException, ClassNotFoundException {
        String find_user="SELECT * FROM computer.ban_list";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_user);
        ResultSet res = preparedStatement.executeQuery();
        List<BanList>list=new ArrayList<>();
        while (res.next()){
            BanList banList=new BanList();
            banList.setId(res.getInt("id"));
            banList.setId_account(res.getInt("id_account"));
            banList.setId_admin(res.getInt("id_admin"));
            banList.setReason(res.getString("reason"));
            banList.setDate(res.getString("date_ban"));
            list.add(banList);
        }
        return list;
    }
    public List<Technique>getAllTechnique() throws SQLException, ClassNotFoundException {
        String find_user="SELECT * FROM computer.technique";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(find_user);
        ResultSet res = preparedStatement.executeQuery();
        List<Technique>list=new ArrayList<>();
        while (res.next()){
            Technique technique=new Technique();
            technique.setId(res.getInt("id"));
            technique.setDate_reg(res.getString("date_take"));
            technique.setPhoto(res.getString("photo"));
            technique.setStatus(res.getString("status"));
            technique.setName(res.getString("name_technique"));
            technique.setType(res.getString("type"));
            technique.setPrice(res.getDouble("price"));
            technique.setNumber(res.getInt("number"));
            list.add(technique);
        }
        return list;
    }
    public void updateStatusAccount(int id , String status1) throws SQLException, ClassNotFoundException {
        String query="UPDATE computer.account SET computer.account.status = '"+status1+"' WHERE computer.account.id = "+id;
        PreparedStatement statement=getDbconnection().prepareStatement(query);
        statement.executeUpdate();
    }
    public void updeteRoleAccount(int id,String role) throws SQLException, ClassNotFoundException {
        String query="UPDATE computer.account SET computer.account.role = '"+role+"' WHERE computer.account.id = "+id;
        PreparedStatement statement=getDbconnection().prepareStatement(query);
        statement.executeUpdate();
    }
    public void registrationAccount(Account account, Person person) throws SQLException, ClassNotFoundException {
        String insert_user="INSERT INTO computer.account( login, password, status, role, date_registration, photo) "+"VALUES(?,?,?,?,?,?)";
        PreparedStatement statement_account=getDbconnection().prepareStatement(insert_user,PreparedStatement.RETURN_GENERATED_KEYS);
        statement_account.setString(1,account.getLogin());
        statement_account.setString(2,account.getPassword());
        statement_account.setString(3,account.getStatus());
        statement_account.setString(4,account.getRole());
        statement_account.setString(5,account.getDate_registration());
        statement_account.setString(6,account.getPhoto());
        statement_account.executeUpdate();
        ResultSet res=statement_account.getGeneratedKeys();
        res.next();
        String insert_personal_info="INSERT INTO computer.person( id_account, name, sur_name, patronymic, phone, country) "+"VALUES(?,?,?,?,?,?)";
        PreparedStatement statement_personal_info=getDbconnection().prepareStatement(insert_personal_info);
        person.setId_account(res.getInt(1));
        statement_personal_info.setInt(1,person.getId_account());
        statement_personal_info.setString(2,person.getName());
        statement_personal_info.setString(3,person.getSur_name());
        statement_personal_info.setString(4,person.getPatronymic());
        statement_personal_info.setString(5,person.getPhone());
        statement_personal_info.setString(6,person.getCountry());
        statement_personal_info.executeUpdate();
    }
    public void addBanList(BanList banList) throws SQLException, ClassNotFoundException {
        String insert_user="INSERT INTO computer.ban_list( reason, date_ban, id_account, id_admin) "+"VALUES(?,?,?,?)";
        PreparedStatement statement_user=getDbconnection().prepareStatement(insert_user,PreparedStatement.RETURN_GENERATED_KEYS);
        statement_user.setString(1,banList.getReason());
        statement_user.setString(2,banList.getDate());
        statement_user.setInt(3,banList.getId_account());
        statement_user.setInt(4,banList.getId_admin());
        statement_user.executeUpdate();
    }
    public void addComputer(Computer computer,Technique technique) throws SQLException, ClassNotFoundException {
        String insert_tech="insert into computer.technique(name_technique, type, number, status, date_take, price,photo) values (?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(insert_tech,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1,technique.getName());
        preparedStatement.setString(2,technique.getType());
        preparedStatement.setInt(3,technique.getNumber());
        preparedStatement.setString(4,technique.getStatus());
        preparedStatement.setString(5,technique.getDate_reg());
        preparedStatement.setDouble(6,technique.getPrice());
        preparedStatement.setString(7,technique.getPhoto());
        preparedStatement.executeUpdate();
        ResultSet res=preparedStatement.getGeneratedKeys();
        res.next();
        computer.setId_technique(res.getInt(1));
        String insert_user="insert into computer.computer(id_technique, video_card, processor, memory, power_unit, color)values(?,?,?,?,?,?) ";
        PreparedStatement statement_account=getDbconnection().prepareStatement(insert_user);
        statement_account.setInt(1,computer.getId_technique());
        statement_account.setString(2,computer.getVideo_card());
        statement_account.setString(3,computer.getCpu());
        statement_account.setString(4,computer.getMemory());
        statement_account.setString(5,computer.getPower_unit());
        statement_account.setString(6,computer.getColor());
        statement_account.executeUpdate();
    }
    public void addVideoCard(VideoCard videoCard , Technique technique) throws SQLException, ClassNotFoundException {
        String insert_tech="insert into computer.technique(name_technique, type, number, status, date_take, price,photo) values (?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(insert_tech,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1,technique.getName());
        preparedStatement.setString(2,technique.getType());
        preparedStatement.setInt(3,technique.getNumber());
        preparedStatement.setString(4,technique.getStatus());
        preparedStatement.setString(5,technique.getDate_reg());
        preparedStatement.setDouble(6,technique.getPrice());
        preparedStatement.setString(7,technique.getPhoto());
        preparedStatement.executeUpdate();
        ResultSet res=preparedStatement.getGeneratedKeys();
        res.next();
        videoCard.setId_technique(res.getInt(1));
        String insert_user="insert into computer.video_card( id_technique, memory_size, type_memory, frequency_memory, frequency_GPU) values (?,?,?,?,?)";
        PreparedStatement statement_account=getDbconnection().prepareStatement(insert_user);
        statement_account.setInt(1,videoCard.getId_technique());
        statement_account.setString(2,videoCard.getMemory_size());
        statement_account.setString(3,videoCard.getType_memory());
        statement_account.setDouble(4,videoCard.getFrequency_memory());
        statement_account.setDouble(5,videoCard.getFrequency_GPU());
        statement_account.executeUpdate();

    }
    public void addCPU(Cpu cpu , Technique technique) throws SQLException, ClassNotFoundException {
        String insert_tech="insert into computer.technique(name_technique, type, number, status, date_take, price,photo) values (?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(insert_tech,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1,technique.getName());
        preparedStatement.setString(2,technique.getType());
        preparedStatement.setInt(3,technique.getNumber());
        preparedStatement.setString(4,technique.getStatus());
        preparedStatement.setString(5,technique.getDate_reg());
        preparedStatement.setDouble(6,technique.getPrice());
        preparedStatement.setString(7,technique.getPhoto());
        preparedStatement.executeUpdate();
        ResultSet res=preparedStatement.getGeneratedKeys();
        res.next();
        cpu.setId_technique(res.getInt(1));
        String insert_user="insert into computer.cpu( id_technique, clock_frequency, performance, energy_consumption, number_cores) values (?,?,?,?,?)";
        PreparedStatement statement_account=getDbconnection().prepareStatement(insert_user);
        statement_account.setInt(1,cpu.getId_technique());
        statement_account.setDouble(2,cpu.getClock_frequency());
        statement_account.setDouble(3,cpu.getPerformance());
        statement_account.setDouble(4,cpu.getEnergy_consumption());
        statement_account.setInt(5,cpu.getNumber_cores());
        statement_account.executeUpdate();
    }
    public void addPowerUnit(PowerUnit powerUnit, Technique technique) throws SQLException, ClassNotFoundException {
        String insert_tech="insert into computer.technique(name_technique, type, number, status, date_take, price,photo) values (?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement=getDbconnection().prepareStatement(insert_tech,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1,technique.getName());
        preparedStatement.setString(2,technique.getType());
        preparedStatement.setInt(3,technique.getNumber());
        preparedStatement.setString(4,technique.getStatus());
        preparedStatement.setString(5,technique.getDate_reg());
        preparedStatement.setDouble(6,technique.getPrice());
        preparedStatement.setString(7,technique.getPhoto());
        preparedStatement.executeUpdate();
        ResultSet res=preparedStatement.getGeneratedKeys();
        res.next();
        powerUnit.setId_technique(res.getInt(1));
        String insert_user="insert into computer.power_unit( id_technique, number_volts, mark) values (?,?,?)";
        PreparedStatement statement_account=getDbconnection().prepareStatement(insert_user);
        statement_account.setInt(1,powerUnit.getId_technique());
        statement_account.setInt(2,powerUnit.getNumber_volts());
        statement_account.setString(3,powerUnit.getMark());
        statement_account.executeUpdate();
    }
}
