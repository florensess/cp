package controllers;

import com.mysql.cj.xdevapi.Client;
import config.DateConfig;
import datebase.DateBase;
import models.*;
import mvc.AccountStatus;
import mvc.ProductStatus;
import mvc.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.List;

public class AdminMenuController {

    private void updateTechnic(ObjectOutputStream out) throws SQLException, ClassNotFoundException, IOException {
        DateBase dateBase=new DateBase();
        List<Technique>list=dateBase.getAllTechnique();
        out.writeObject(list);
    }

    private void updateBanList(ObjectOutputStream out) throws SQLException, ClassNotFoundException, IOException {
        DateBase dateBase=new DateBase();
        List<BanList>list=dateBase.getAllBanList();
        out.writeObject(list);
    }
    private void updateAccount(ObjectOutputStream out,Account account) throws SQLException, ClassNotFoundException, IOException {
        DateBase dateBase=new DateBase();
        Person person=dateBase.findPersonByIdAccount(account.getId());
        List<Account>listAccount=dateBase.getAllAccount();
        List<Person>listPerson=dateBase.getAllPerson();
        out.writeObject(account);
        out.writeObject(person);
        out.writeObject(listAccount);
        out.writeObject(listPerson);
    }
    private void updateStatistic(ObjectOutputStream out) throws SQLException, ClassNotFoundException, IOException {
        DateBase dateBase=new DateBase();
        List<Account> listAccountUser=dateBase.findAllAccountByRole(AccountStatus.user);
        List<Account>listAccountAdmin=dateBase.findAllAccountByRole(AccountStatus.admin);
        List<Account> listBlock=dateBase.findAllBLockAccount(AccountStatus.blockAccount);
        List<Technique>listTech=dateBase.getAllTechnique();
        out.writeObject(Integer.toString(listAccountAdmin.size()));
        out.writeObject(Integer.toString(listAccountUser.size()));
        out.writeObject(Integer.toString(listBlock.size()));
        out.writeObject(Integer.toString(listTech.size()));
    }

    public void start(ObjectOutputStream out, ObjectInputStream in, Account account) throws SQLException, IOException, ClassNotFoundException {
        updateAccount(out, account);
        updateTechnic(out);
        updateStatistic(out);
        updateBanList(out);
        while (true){
            Request request=(Request) in.readObject();
            switch (request){
                case FIND_ACC:findAcc(in, out);break;
                case UPDATE_BAN:updateBanList(out);break;
                case REMOVE_PRODUCT:removeProduct(in, out);break;
                case PERSONAL_TECH:personalTech(in,out);break;
                case UPDATE_TECH:updateTechnic(out);break;
                case ADD_PRODUCT:addProduct(in);break;
                case PERSONAL_ACCOUNT:personalAccount(in, out,account);break;
                case UNBLOCK_ACCOUNT:unblockAccount(in);break;
                case BLOCK_ACCOUNT:blockAccount(in,account);break;
                case SAVE_PARSONAL_INFO:changePersonalInfo(in, account);break;
                case REMOVE_ACCOUNT:removeAccount(in, out, account);break;
                case UPDATE_ACCOUNT:updateAccount(out, account);break;
                case TAKE_ADMIN:takeAdmin(in);break;
                case REMOVE_ROOL_ADMIN:removeRole(in);break;
                case EXIT:AutorizationMenuController autorizationMenuController=new AutorizationMenuController();autorizationMenuController.start(out,in);
            }
        }
    }
    private void findAcc(ObjectInputStream in,ObjectOutputStream out) throws IOException, ClassNotFoundException, SQLException {
        int id=(int)in.readObject();
        DateBase dateBase=new DateBase();
        Account account=dateBase.findAccountById(id);
        out.writeObject(account);
    }
    private void removeRole(ObjectInputStream in) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        int id=(int) in.readObject();
        dateBase.updeteRoleAccount(id,AccountStatus.user);
    }
    private void takeAdmin(ObjectInputStream in) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        int id=(int)in.readObject();
        dateBase.updeteRoleAccount(id,AccountStatus.admin);
    }
    private void removeProduct(ObjectInputStream in,ObjectOutputStream out) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        dateBase.removeProduct((int)in.readObject());
    }
    private void personalTech(ObjectInputStream in,ObjectOutputStream out) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        int idTech=(int)in.readObject();
        Technique technique=dateBase.findTechniqueById(idTech);
        out.writeObject(technique);
        if(technique.getType().equals(ProductStatus.computerProduct)){
            Computer computer=dateBase.findComputerByIdTechnique(technique.getId());
            out.writeObject(computer);
        }
        if(technique.getType().equals(ProductStatus.computerVideo)){
            VideoCard videoCard=dateBase.findVideoCardByIdTechnique(technique.getId());
            out.writeObject(videoCard);
        }
        if(technique.getType().equals(ProductStatus.computerCPU)){
            Cpu cpu=dateBase.findCPUByIdTechnique(technique.getId());
            out.writeObject(cpu);
        }
        if(technique.getType().equals(ProductStatus.computerPower)){
            PowerUnit powerUnit=dateBase.findPowerByIdTechnique(technique.getId());
            out.writeObject(powerUnit);
        }
    }
    private void addProduct(ObjectInputStream in) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        Technique technique=(Technique) in.readObject();
        technique.setDate_reg(DateConfig.getDateConfigure().nowDate());
        technique.setStatus(ProductStatus.activeProduct);
        if(technique.getType().equals(ProductStatus.computerProduct)){
            Computer computer=(Computer) in.readObject();
            dateBase.addComputer(computer,technique);
        }
        if(technique.getType().equals(ProductStatus.computerVideo)){
            VideoCard videoCard=(VideoCard) in.readObject();
            dateBase.addVideoCard(videoCard,technique);
        }
        if(technique.getType().equals(ProductStatus.computerCPU)){
            Cpu cpu=(Cpu) in.readObject();
            dateBase.addCPU(cpu,technique);
        }
        if(technique.getType().equals(ProductStatus.computerPower)){
            PowerUnit powerUnit=(PowerUnit) in.readObject();
            dateBase.addPowerUnit(powerUnit,technique);
        }
        if(technique.getType().equals(ProductStatus.computerAct)){

        }
    }
    private void personalAccount(ObjectInputStream in,ObjectOutputStream out,Account account1) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        int id=(int)in.readObject();
        Person person=(Person) dateBase.findPersonByIdAccount(id);
        Account account=(Account) dateBase.findAccountById(id);
        out.writeObject(account);
        out.writeObject(person);
        if(id==account1.getId()){
            out.writeObject(Request.YOUR_ACCOUNT);
        }
        else out.writeObject(Request.UPDATE_ACCOUNT);
    }
    private void unblockAccount(ObjectInputStream in) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        int id_account=(int)in.readObject();
        dateBase.removeRecodsBanList(id_account);
        dateBase.updateStatusAccount(id_account,AccountStatus.activeAccount);
    }
    private void blockAccount(ObjectInputStream in,Account id_acmin ) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        BanList banList=new BanList();
        int id_account=(int)in.readObject();
        String reasin=(String)in.readObject();
        if(reasin.equals("")){
            reasin="Причина не указана";
        }
        banList.setId_account(id_account);
        banList.setId_admin(id_acmin.getId());
        banList.setReason(reasin);
        banList.setDate(DateConfig.getDateConfigure().nowDate());
        dateBase.addBanList(banList);
        dateBase.updateStatusAccount(id_account,AccountStatus.blockAccount);
    }
    private void changePersonalInfo(ObjectInputStream in ,Account account) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        Account accountNew=(Account) in.readObject();
        Person personNew=(Person)in.readObject();
        if(accountNew.getPhoto()==null){
            accountNew.setPhoto(account.getPhoto());
        }
        else account.setPhoto(accountNew.getPhoto());
        accountNew.setLogin(accountNew.getLogin());
        account.setPassword(accountNew.getPassword());
        accountNew.setId(account.getId());
        personNew.setId_account(account.getId());
        dateBase.updateAccountInfo(accountNew,personNew);
    }
    private void removeAccount(ObjectInputStream in,ObjectOutputStream out,Account account) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        dateBase.removeAccount((int) in.readObject());
        //updateAccount(out, account);
    }
}
