package controllers;

import config.DateConfig;
import datebase.DateBase;
import models.Account;
import models.Person;
import mvc.AccountStatus;
import mvc.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutorizationMenuController {
    public void start(ObjectOutputStream out, ObjectInputStream in) throws IOException, ClassNotFoundException, SQLException {
        while (true) {
            Request request = (Request) in.readObject();
            switch (request) {
                case AUTORIZATION:autorization(in,out);break;
                case REGISTRATION_ACCOUNT:registration(in);break;
            }
        }
    }
    public void autorization(ObjectInputStream in,ObjectOutputStream out) throws IOException, ClassNotFoundException, SQLException {
        DateBase datebase=new DateBase();
        Account user=new Account();
        user.setLogin((String) in.readObject());
        user.setPassword((String) in.readObject());
        user=datebase.autorizationRequest(user);
        if(user.getRole()==null){
            out.writeObject(Request.AUTORIZATION_NOT_FOUND_ACCOUNT);
        }
        else {
            out.writeObject(Request.AUTORIZATION_SEC_FOUND);
            if(user.getStatus().equals(AccountStatus.blockAccount)){
                out.writeObject(Request.AUTORIZATION_BLOCK_ACCOUNT);
            }
            else {
                if(user.getRole().equals(AccountStatus.admin)){
                    out.writeObject(Request.AUTORIZATION_ADMIN);
                   AdminMenuController adminMenuController=new AdminMenuController();
                   adminMenuController.start(out,in,user);
                }
                if(user.getRole().equals(AccountStatus.user)){
                    out.writeObject(Request.AUTORIZATION_USER);
                    UserMenuController userMenuController=new UserMenuController();
                    userMenuController.start(out,in,user);
                }
            }
        }
    }
    private void registration(ObjectInputStream in) throws IOException, ClassNotFoundException, SQLException {
        DateBase datebase=new DateBase();
        Account user=(Account) in.readObject();
        Person patient=(Person) in.readObject();
        user.setRole(AccountStatus.user);
        user.setStatus(AccountStatus.activeAccount);
        user.setDate_registration(DateConfig.getDateConfigure().nowDate());
        datebase.registrationAccount(user,patient);
    }
}
