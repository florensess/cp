package controllers;

import datebase.DateBase;
import models.Account;
import models.Person;
import models.Technique;
import mvc.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.List;

public class UserMenuController {

    private void intializedTech(ObjectOutputStream out) throws SQLException, ClassNotFoundException, IOException {
        DateBase dateBase=new DateBase();
        List<Technique>list=dateBase.getAllTechnique();
        out.writeObject(list);
    }

     public void start(ObjectOutputStream out, ObjectInputStream in, Account account) throws IOException, ClassNotFoundException, SQLException {
         intializedTech(out);
            while(true){
                Request request=(Request) in.readObject();
                switch (request){
                    case EXIT:AutorizationMenuController autorizationMenuController=new AutorizationMenuController();autorizationMenuController.start(out, in);
                }
            }
     }
    private void changePersonalInfo(ObjectInputStream in ,Account account) throws IOException, ClassNotFoundException, SQLException {
        DateBase dateBase=new DateBase();
        Account accountNew=(Account) in.readObject();
        Person personNew=(Person)in.readObject();
        if(accountNew.getPhoto()==null){
            accountNew.setPhoto(account.getPhoto());
        }
        else account.setPhoto(accountNew.getPhoto());
        accountNew.setLogin(accountNew.getLogin());
        account.setPassword(accountNew.getPassword());
        accountNew.setId(account.getId());
        personNew.setId_account(account.getId());
        dateBase.updateAccountInfo(accountNew,personNew);
    }
}
